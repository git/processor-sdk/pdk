/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
*  \file sciclient_defaultBoardcfg_tifs_rm_hexhs.h
*
*  \brief File containing the Binary in a C array.
*
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#ifndef SCICLIENT_DEFAULTBOARDCFG_TIFS_RM_HEXHS_H_
#define SCICLIENT_DEFAULTBOARDCFG_TIFS_RM_HEXHS_H_


#define SCICLIENT_BOARDCFG_TIFS_RM_SIZE_IN_BYTES (3713U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define SCICLIENT_BOARDCFG_TIFS_RM { \
    0x67068230U,     0x4f048230U,     0x010203a0U,     0x4c140202U, \
    0xa75e1082U,     0xe181d79aU,     0xdc863378U,     0x341006beU, \
    0x309d9d83U,     0x2a09060dU,     0xf7864886U,     0x0d01010dU, \
    0x81300005U,     0x300b318bU,     0x55030609U,     0x02130604U, \
    0x0b315355U,     0x03060930U,     0x0c080455U,     0x31435302U, \
    0x060d300fU,     0x07045503U,     0x6144060cU,     0x73616c6cU, \
    0x1f302131U,     0x04550306U,     0x54180c0aU,     0x73617865U, \
    0x736e4920U,     0x6d757274U,     0x73746e65U,     0x49202c2eU, \
    0x312e636eU,     0x060a300cU,     0x0b045503U,     0x4250030cU, \
    0x300f3155U,     0x5503060dU,     0x060c0304U,     0x65626c41U, \
    0x1c317472U,     0x09061a30U,     0x8648862aU,     0x09010df7U, \
    0x410d1601U,     0x7265626cU,     0x69744074U,     0x6d6f632eU, \
    0x0d171e30U,     0x33303532U,     0x34303430U,     0x39353434U, \
    0x320d175aU,     0x30343035U,     0x34343033U,     0x5a393534U, \
    0x318b8130U,     0x0609300bU,     0x06045503U,     0x53550213U, \
    0x09300b31U,     0x04550306U,     0x53020c08U,     0x300f3143U, \
    0x5503060dU,     0x060c0704U,     0x6c6c6144U,     0x21317361U, \
    0x03061f30U,     0x0c0a0455U,     0x78655418U,     0x49207361U, \
    0x7274736eU,     0x6e656d75U,     0x2c2e7374U,     0x636e4920U, \
    0x300c312eU,     0x5503060aU,     0x030c0b04U,     0x31554250U, \
    0x060d300fU,     0x03045503U,     0x6c41060cU,     0x74726562U, \
    0x1a301c31U,     0x862a0906U,     0x0df78648U,     0x16010901U, \
    0x626c410dU,     0x40747265U,     0x632e6974U,     0x82306d6fU, \
    0x0d302202U,     0x862a0906U,     0x0df78648U,     0x05010101U, \
    0x02820300U,     0x8230000fU,     0x82020a02U,     0xbf000102U, \
    0xd849ae14U,     0x6bd3727fU,     0x48ebcd23U,     0x22dc650eU, \
    0x4f0ef24dU,     0xb5edf682U,     0x7cdbddf2U,     0x596efa91U, \
    0xb6f7d5ffU,     0x8a1d04deU,     0xd995d2ccU,     0xc1c4e0d1U, \
    0xffbf50f8U,     0x22910c48U,     0x7b4c9a50U,     0x0a96f38bU, \
    0xa4b32628U,     0x55a9e0d9U,     0x3efb1a41U,     0xbf6c275bU, \
    0xaf71c0caU,     0xee22722fU,     0x62250146U,     0x04c73eadU, \
    0xb618b1f6U,     0x6e12c02cU,     0x3e9be20fU,     0xa8a0a6e5U, \
    0x41034506U,     0x1f164e17U,     0x84d674a9U,     0xa779d64eU, \
    0xa911b810U,     0x251f920eU,     0xf2b17fddU,     0x68f2b9d1U, \
    0x4b5933d8U,     0xcc777d82U,     0x23fa9cd1U,     0x8858fbb4U, \
    0xd5eacdf2U,     0x752cf216U,     0xc362fa2dU,     0xe06e09c1U, \
    0xb5e07006U,     0x62990907U,     0xe7e4d6d9U,     0x82c86d6cU, \
    0xf7935007U,     0xd1edd8e2U,     0x9ed0e35fU,     0xd95493cfU, \
    0xce5ddd5fU,     0xabf16037U,     0x7b048a14U,     0x7fbaa765U, \
    0x7c4545dfU,     0xae5ba14bU,     0x3d94c64eU,     0xd2874e8cU, \
    0xf3a43c94U,     0xf2fcda9fU,     0x0de77c36U,     0x37425aadU, \
    0xd0812af1U,     0x67a7a16eU,     0xed871e03U,     0x4a73bb00U, \
    0xa2312868U,     0x04a39a82U,     0xff87e8c1U,     0xc1aa7e45U, \
    0x053bd49fU,     0x21fd83c7U,     0x7fbdfe71U,     0x1916c938U, \
    0x03e60e52U,     0x1e1d8d33U,     0xcd1c36c9U,     0x29829d4eU, \
    0x2a9bcd88U,     0x7b5f6cbeU,     0x793ab2b2U,     0xf57d6a00U, \
    0x1e9d1aadU,     0xcf2a58cdU,     0x804ef45eU,     0xdd4f3babU, \
    0x34ded4f8U,     0xd920c4a2U,     0x852d1959U,     0x681f5e02U, \
    0xb98d4cb1U,     0x2de90611U,     0x8c58b576U,     0x6e37a850U, \
    0x836f7866U,     0x344d4630U,     0x4a18b49fU,     0x7bfabbb9U, \
    0x32d6aec5U,     0x6c848410U,     0x33809a3fU,     0xbc4dfc35U, \
    0x54606ed5U,     0x6d7ecf50U,     0xfa049780U,     0xfd200b8fU, \
    0xa12b98bdU,     0xfd59bd37U,     0xa145ec4aU,     0xc9178b09U, \
    0xb7331472U,     0x5d125e05U,     0xce1d5ae2U,     0xe1f65421U, \
    0xaa55d5eaU,     0x094deb27U,     0x824019dfU,     0x1789662eU, \
    0xb36ed965U,     0x8d4e38d6U,     0x74d61661U,     0x5f16ded4U, \
    0x42d51951U,     0xc8d283b8U,     0x69a94bdeU,     0x028db697U, \
    0x01000103U,     0x30c081a3U,     0x0c30bd81U,     0x1d550306U, \
    0x30050413U,     0xff010103U,     0x09061230U,     0x0401062bU, \
    0x01268201U,     0x30050403U,     0x01010203U,     0x09066030U, \
    0x0401062bU,     0x01268201U,     0x30530422U,     0x60090651U, \
    0x65014886U,     0x03020403U,     0xe4194004U,     0x7a8fcf8bU, \
    0x28812e2fU,     0x839412d0U,     0x827329c1U,     0xa1518564U, \
    0xc118aeceU,     0x7149e2a4U,     0x44c8dd81U,     0xa52b84c9U, \
    0x7bab14d5U,     0x25c24e87U,     0xf418dbd1U,     0x36454ed1U, \
    0x8b4920eeU,     0xe92832d4U,     0x020241e7U,     0x18301608U, \
    0x062b0906U,     0x82010401U,     0x04230126U,     0x0409300bU, \
    0x00000004U,     0x02010200U,     0x03061d30U,     0x040e1d55U, \
    0x04140416U,     0x84b3bbdfU,     0x0c257714U,     0xa4332ecbU, \
    0x59854d7dU,     0x30e5fb12U,     0x2a09060dU,     0xf7864886U, \
    0x0d01010dU,     0x82030005U,     0x3d000102U,     0x3e791a77U, \
    0xfd896df4U,     0xfcdb0551U,     0x26346be0U,     0x05b96052U, \
    0xa4e46dcbU,     0x14478f78U,     0x39fa0986U,     0xbeb2924bU, \
    0xce050727U,     0xd0f25ba4U,     0x2aba8664U,     0x7de5f5e0U, \
    0x9585d293U,     0xf39bb85cU,     0x0ae8ed4fU,     0xb60b1820U, \
    0xa5c8d8bfU,     0x29fa37f1U,     0xf7fb2c47U,     0x41944b9fU, \
    0x4113e206U,     0x017a7217U,     0x1c55c872U,     0xfc5ea2bfU, \
    0x00232203U,     0xaf4b1beaU,     0xf2a4ebccU,     0x1212a8b7U, \
    0x97ec354aU,     0x26fc35b8U,     0x4a89e7e8U,     0x00c7228dU, \
    0x4301932aU,     0x7c92a36fU,     0x89683a09U,     0x33a66d91U, \
    0x42c9c5d8U,     0xb64e1d26U,     0xeea3acedU,     0xa146780fU, \
    0xdf988b0fU,     0x280d8c23U,     0x3f2248e1U,     0xa576ceccU, \
    0xb53ac1f9U,     0xfbec46d7U,     0x1a1ac8c2U,     0x89393ba2U, \
    0x36ca0a94U,     0x3f426a05U,     0x79c6d845U,     0x2d6987f2U, \
    0x2f7f251aU,     0x2110c472U,     0x59637830U,     0x9fabe8b8U, \
    0x9abe3b63U,     0x7d31e39dU,     0xe6146261U,     0xd3878ae5U, \
    0xe741b38aU,     0xeca9528aU,     0xa2fa6c59U,     0x7bc3f1d3U, \
    0xd6ceb146U,     0x512df1afU,     0xcf1e4712U,     0xb1668c2fU, \
    0x54153508U,     0x3eeb5738U,     0x21c67192U,     0xf7cc7b28U, \
    0x2159d28eU,     0x8ad78457U,     0x69b3b198U,     0xb2519405U, \
    0xafdd6d3cU,     0xe40c9a22U,     0xca990a19U,     0x56869624U, \
    0x8c89a281U,     0x19ad66f2U,     0xcebdc144U,     0x4efe344eU, \
    0xdf77a38fU,     0xb194a8a0U,     0x96c8505bU,     0x4763d4ddU, \
    0x9b293a38U,     0xda718f9aU,     0x76bc0ad1U,     0xa5877850U, \
    0xd82d5534U,     0xa7b819ccU,     0x6ace01dfU,     0x32d3c274U, \
    0x7c8c798fU,     0xe50835bbU,     0x00b1cf68U,     0x976b8998U, \
    0xaaa50511U,     0x1e6595abU,     0x42289f17U,     0xc610c8f8U, \
    0x9c958c58U,     0x838c5400U,     0x80185150U,     0xc76a66ccU, \
    0xdedc0be2U,     0xb8aa1925U,     0xfcd73bd2U,     0x520904b5U, \
    0x302f9263U,     0x152e0268U,     0xba34c32bU,     0xc05f2b74U, \
    0xab604167U,     0xba8aa34bU,     0x5cf2e02dU,     0x2e1cf410U, \
    0xd56d6979U,     0x24e35a51U,     0xf404c434U,     0x3f153453U, \
    0xa8377a49U,     0x15b5c549U,     0x00547e26U,     0x644c4101U, \
    0xaa2a0301U,     0xaaaaaaaaU,     0xaaaaaaaaU,     0xaaaa2a05U, \
    0xaaaaaaaaU,     0x0caaaaaaU,     0xaaaaaa2aU,     0xaaaaaaaaU, \
    0x2a0daaaaU,     0xaaaaaaaaU,     0xaaaaaaaaU,     0xaa2a23aaU, \
    0xaaaaaaaaU,     0xaaaaaaaaU,     0xaaaa2a25U,     0xaaaaaaaaU, \
    0x00aaaaaaU,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x087b2500U,     0x0006a800U,     0x40000000U,     0x8033ca00U, \
    0x40000000U,     0x80340a00U,     0x56001200U,     0x0c344a00U, \
    0x20006800U,     0x0d344a00U,     0x10008800U,     0x03344a00U, \
    0x10009800U,     0x05344a00U,     0x2000a800U,     0x23344a00U, \
    0x1800c800U,     0x25344a00U,     0x2000e000U,     0x80344a00U, \
    0x04000000U,     0x0c348000U,     0x04000400U,     0x0d348000U, \
    0x04000800U,     0x03348000U,     0x04000c00U,     0x05348000U, \
    0x10001000U,     0x23348000U,     0x10002000U,     0x25348000U, \
    0x10003000U,     0x80348000U,     0x01000000U,     0x8034c000U, \
    0xc8007800U,     0x0c34c100U,     0x28014000U,     0x0d34c100U, \
    0x20016800U,     0x0334c100U,     0x20018800U,     0x0534c100U, \
    0x0001a800U,     0x2334c101U,     0x0002a800U,     0x2534c101U, \
    0x2603a800U,     0x8034c100U,     0x04004000U,     0x0c34c200U, \
    0x02004400U,     0x0d34c200U,     0x02004600U,     0x0334c200U, \
    0x02004800U,     0x0534c200U,     0x02004a00U,     0x2334c200U, \
    0x02004c00U,     0x2534c200U,     0x14004e00U,     0x0c34c200U, \
    0x04006200U,     0x0d34c200U,     0x08006600U,     0x2334c200U, \
    0x08006e00U,     0x2534c200U,     0x02007600U,     0x8034c200U, \
    0x04000400U,     0x0c34c300U,     0x02000800U,     0x0d34c300U, \
    0x02000a00U,     0x0334c300U,     0x02000c00U,     0x0534c300U, \
    0x02000e00U,     0x2334c300U,     0x02001000U,     0x2534c300U, \
    0x14001200U,     0x0c34c300U,     0x04002600U,     0x0d34c300U, \
    0x08002a00U,     0x2334c300U,     0x08003200U,     0x2534c300U, \
    0x02003a00U,     0x8034c300U,     0x00003e00U,     0x0c34c500U, \
    0x01003e00U,     0x0c34c500U,     0x00003e00U,     0x2334c500U, \
    0x01003f00U,     0x2334c500U,     0x00003c00U,     0x2534c600U, \
    0x02003c00U,     0x2534c600U,     0x00000200U,     0x0c34c700U, \
    0x01000200U,     0x0c34c700U,     0x00000200U,     0x2334c700U, \
    0x01000300U,     0x2334c700U,     0x00000000U,     0x2534c800U, \
    0x02000000U,     0x2534c800U,     0x01000200U,     0x0c34ca00U, \
    0x01000300U,     0x0d34ca00U,     0x03000000U,     0x0c34cb00U, \
    0x02000300U,     0x0d34cb00U,     0x01000500U,     0x0334cb00U, \
    0x01000600U,     0x0534cb00U,     0x10000700U,     0x2334cb00U, \
    0x08001700U,     0x2534cb00U,     0x01001f00U,     0x8034cb00U, \
    0x08003c00U,     0x0c350000U,     0x08004400U,     0x0d350000U, \
    0x08004c00U,     0x25350000U,     0x42005400U,     0x80350000U, \
    0x01000000U,     0x80350100U,     0x04000400U,     0x0c350a00U, \
    0x02000800U,     0x0d350a00U,     0x02000a00U,     0x03350a00U, \
    0x02000c00U,     0x05350a00U,     0x02000e00U,     0x23350a00U, \
    0x02001000U,     0x25350a00U,     0x14001200U,     0x0c350a00U, \
    0x04002600U,     0x0d350a00U,     0x08002a00U,     0x23350a00U, \
    0x08003200U,     0x25350a00U,     0x02003a00U,     0x80350a00U, \
    0x00000200U,     0x0c350b00U,     0x01000200U,     0x0c350b00U, \
    0x00000200U,     0x23350b00U,     0x01000300U,     0x23350b00U, \
    0x00000000U,     0x25350c00U,     0x02000000U,     0x25350c00U, \
    0x04000400U,     0x0c350d00U,     0x02000800U,     0x0d350d00U, \
    0x02000a00U,     0x03350d00U,     0x02000c00U,     0x05350d00U, \
    0x02000e00U,     0x23350d00U,     0x02001000U,     0x25350d00U, \
    0x14001200U,     0x0c350d00U,     0x04002600U,     0x0d350d00U, \
    0x08002a00U,     0x23350d00U,     0x08003200U,     0x25350d00U, \
    0x02003a00U,     0x80350d00U,     0x00000200U,     0x0c350f00U, \
    0x01000200U,     0x0c350f00U,     0x00000200U,     0x23350f00U, \
    0x01000300U,     0x23350f00U,     0x00000000U,     0x25351000U, \
    0x02000000U,     0x25351000U,     0x20000f00U,     0x0c3a4a00U, \
    0x10002f00U,     0x0d3a4a00U,     0x40003f00U,     0x033a4a00U, \
    0x20007f00U,     0x053a4a00U,     0x10009f00U,     0x233a4a00U, \
    0x1000af00U,     0x253a4a00U,     0x4100bf00U,     0x803a4a00U, \
    0x08000100U,     0x0c3a8000U,     0x04000900U,     0x0d3a8000U, \
    0x10000d00U,     0x033a8000U,     0x10001d00U,     0x053a8000U, \
    0x08002d00U,     0x233a8000U,     0x08003500U,     0x253a8000U, \
    0x03003d00U,     0x803a8000U,     0x01000000U,     0x803ac000U, \
    0x20006000U,     0x0c3ac100U,     0x10008000U,     0x0d3ac100U, \
    0x20009000U,     0x033ac100U,     0x2000b000U,     0x053ac100U, \
    0x1000d000U,     0x233ac100U,     0x1000e000U,     0x253ac100U, \
    0x0c00f000U,     0x803ac100U,     0x03003200U,     0x0c3ac200U, \
    0x02003500U,     0x0d3ac200U,     0x02003700U,     0x033ac200U, \
    0x02003900U,     0x053ac200U,     0x02003b00U,     0x233ac200U, \
    0x02003d00U,     0x253ac200U,     0x09003f00U,     0x0c3ac200U, \
    0x04004800U,     0x0d3ac200U,     0x04004c00U,     0x033ac200U, \
    0x04005000U,     0x053ac200U,     0x04005400U,     0x233ac200U, \
    0x04005800U,     0x253ac200U,     0x01005c00U,     0x803ac200U, \
    0x03000200U,     0x0c3ac300U,     0x02000500U,     0x0d3ac300U, \
    0x02000700U,     0x033ac300U,     0x02000900U,     0x053ac300U, \
    0x02000b00U,     0x233ac300U,     0x02000d00U,     0x253ac300U, \
    0x09000f00U,     0x0c3ac300U,     0x04001800U,     0x0d3ac300U, \
    0x04001c00U,     0x033ac300U,     0x04002000U,     0x053ac300U, \
    0x04002400U,     0x233ac300U,     0x04002800U,     0x253ac300U, \
    0x02002c00U,     0x803ac300U,     0x00003000U,     0x033ac500U, \
    0x00003000U,     0x0c3ac500U,     0x01003000U,     0x0c3ac500U, \
    0x01003100U,     0x033ac500U,     0x00000000U,     0x033ac700U, \
    0x00000000U,     0x0c3ac700U,     0x01000000U,     0x0c3ac700U, \
    0x01000100U,     0x033ac700U,     0x01000200U,     0x0c3aca00U, \
    0x01000300U,     0x0d3aca00U,     0x03000000U,     0x0c3acb00U, \
    0x02000300U,     0x0d3acb00U,     0x06000500U,     0x033acb00U, \
    0x06000b00U,     0x053acb00U,     0x05001100U,     0x233acb00U, \
    0x05001600U,     0x253acb00U,     0x05001b00U,     0x803acb00U, \
    0x08003000U,     0x0c3b0000U,     0x04003800U,     0x0d3b0000U, \
    0x08003c00U,     0x033b0000U,     0x04004400U,     0x053b0000U, \
    0x08004800U,     0x233b0000U,     0x04005000U,     0x253b0000U, \
    0x0c005400U,     0x803b0000U,     0x01000000U,     0x803b0100U, \
    0x03000200U,     0x0c3b0a00U,     0x02000500U,     0x0d3b0a00U, \
    0x02000700U,     0x033b0a00U,     0x02000900U,     0x053b0a00U, \
    0x02000b00U,     0x233b0a00U,     0x02000d00U,     0x253b0a00U, \
    0x09000f00U,     0x0c3b0a00U,     0x04001800U,     0x0d3b0a00U, \
    0x04001c00U,     0x033b0a00U,     0x04002000U,     0x053b0a00U, \
    0x04002400U,     0x233b0a00U,     0x04002800U,     0x253b0a00U, \
    0x01002c00U,     0x803b0a00U,     0x00000000U,     0x033b0b00U, \
    0x00000000U,     0x0c3b0b00U,     0x01000000U,     0x0c3b0b00U, \
    0x01000100U,     0x033b0b00U,     0x03000200U,     0x0c3b0d00U, \
    0x02000500U,     0x0d3b0d00U,     0x02000700U,     0x033b0d00U, \
    0x02000900U,     0x053b0d00U,     0x02000b00U,     0x233b0d00U, \
    0x02000d00U,     0x253b0d00U,     0x09000f00U,     0x0c3b0d00U, \
    0x04001800U,     0x0d3b0d00U,     0x04001c00U,     0x033b0d00U, \
    0x04002000U,     0x053b0d00U,     0x04002400U,     0x233b0d00U, \
    0x04002800U,     0x253b0d00U,     0x02002c00U,     0x803b0d00U, \
    0x00000000U,     0x033b0f00U,     0x00000000U,     0x0c3b0f00U, \
    0x01000000U,     0x0c3b0f00U,     0x01000100U,     0x033b0f00U, \
    0x00000000U\
} /* 3713 bytes */

#endif
