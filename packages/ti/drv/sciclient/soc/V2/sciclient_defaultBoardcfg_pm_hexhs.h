/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
*  \file sciclient_defaultBoardcfg_pm_hexhs.h
*
*  \brief File containing the Binary in a C array.
*
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#ifndef SCICLIENT_DEFAULTBOARDCFG_PM_HEXHS_H_
#define SCICLIENT_DEFAULTBOARDCFG_PM_HEXHS_H_


#define SCICLIENT_BOARDCFG_PM_SIZE_IN_BYTES (1644U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define SCICLIENT_BOARDCFG_PM { \
    0x66068230U,     0x4e048230U,     0x010203a0U,     0x36140202U, \
    0x4071a339U,     0x45d8fcb5U,     0x3fd5a857U,     0xf99521ddU, \
    0x30cc3ac9U,     0x2a09060dU,     0xf7864886U,     0x0d01010dU, \
    0x81300005U,     0x300b318bU,     0x55030609U,     0x02130604U, \
    0x0b315355U,     0x03060930U,     0x0c080455U,     0x31435302U, \
    0x060d300fU,     0x07045503U,     0x6144060cU,     0x73616c6cU, \
    0x1f302131U,     0x04550306U,     0x54180c0aU,     0x73617865U, \
    0x736e4920U,     0x6d757274U,     0x73746e65U,     0x49202c2eU, \
    0x312e636eU,     0x060a300cU,     0x0b045503U,     0x4250030cU, \
    0x300f3155U,     0x5503060dU,     0x060c0304U,     0x65626c41U, \
    0x1c317472U,     0x09061a30U,     0x8648862aU,     0x09010df7U, \
    0x410d1601U,     0x7265626cU,     0x69744074U,     0x6d6f632eU, \
    0x0d171e30U,     0x33303532U,     0x34303430U,     0x39353434U, \
    0x320d175aU,     0x30343035U,     0x34343033U,     0x5a393534U, \
    0x318b8130U,     0x0609300bU,     0x06045503U,     0x53550213U, \
    0x09300b31U,     0x04550306U,     0x53020c08U,     0x300f3143U, \
    0x5503060dU,     0x060c0704U,     0x6c6c6144U,     0x21317361U, \
    0x03061f30U,     0x0c0a0455U,     0x78655418U,     0x49207361U, \
    0x7274736eU,     0x6e656d75U,     0x2c2e7374U,     0x636e4920U, \
    0x300c312eU,     0x5503060aU,     0x030c0b04U,     0x31554250U, \
    0x060d300fU,     0x03045503U,     0x6c41060cU,     0x74726562U, \
    0x1a301c31U,     0x862a0906U,     0x0df78648U,     0x16010901U, \
    0x626c410dU,     0x40747265U,     0x632e6974U,     0x82306d6fU, \
    0x0d302202U,     0x862a0906U,     0x0df78648U,     0x05010101U, \
    0x02820300U,     0x8230000fU,     0x82020a02U,     0xbf000102U, \
    0xd849ae14U,     0x6bd3727fU,     0x48ebcd23U,     0x22dc650eU, \
    0x4f0ef24dU,     0xb5edf682U,     0x7cdbddf2U,     0x596efa91U, \
    0xb6f7d5ffU,     0x8a1d04deU,     0xd995d2ccU,     0xc1c4e0d1U, \
    0xffbf50f8U,     0x22910c48U,     0x7b4c9a50U,     0x0a96f38bU, \
    0xa4b32628U,     0x55a9e0d9U,     0x3efb1a41U,     0xbf6c275bU, \
    0xaf71c0caU,     0xee22722fU,     0x62250146U,     0x04c73eadU, \
    0xb618b1f6U,     0x6e12c02cU,     0x3e9be20fU,     0xa8a0a6e5U, \
    0x41034506U,     0x1f164e17U,     0x84d674a9U,     0xa779d64eU, \
    0xa911b810U,     0x251f920eU,     0xf2b17fddU,     0x68f2b9d1U, \
    0x4b5933d8U,     0xcc777d82U,     0x23fa9cd1U,     0x8858fbb4U, \
    0xd5eacdf2U,     0x752cf216U,     0xc362fa2dU,     0xe06e09c1U, \
    0xb5e07006U,     0x62990907U,     0xe7e4d6d9U,     0x82c86d6cU, \
    0xf7935007U,     0xd1edd8e2U,     0x9ed0e35fU,     0xd95493cfU, \
    0xce5ddd5fU,     0xabf16037U,     0x7b048a14U,     0x7fbaa765U, \
    0x7c4545dfU,     0xae5ba14bU,     0x3d94c64eU,     0xd2874e8cU, \
    0xf3a43c94U,     0xf2fcda9fU,     0x0de77c36U,     0x37425aadU, \
    0xd0812af1U,     0x67a7a16eU,     0xed871e03U,     0x4a73bb00U, \
    0xa2312868U,     0x04a39a82U,     0xff87e8c1U,     0xc1aa7e45U, \
    0x053bd49fU,     0x21fd83c7U,     0x7fbdfe71U,     0x1916c938U, \
    0x03e60e52U,     0x1e1d8d33U,     0xcd1c36c9U,     0x29829d4eU, \
    0x2a9bcd88U,     0x7b5f6cbeU,     0x793ab2b2U,     0xf57d6a00U, \
    0x1e9d1aadU,     0xcf2a58cdU,     0x804ef45eU,     0xdd4f3babU, \
    0x34ded4f8U,     0xd920c4a2U,     0x852d1959U,     0x681f5e02U, \
    0xb98d4cb1U,     0x2de90611U,     0x8c58b576U,     0x6e37a850U, \
    0x836f7866U,     0x344d4630U,     0x4a18b49fU,     0x7bfabbb9U, \
    0x32d6aec5U,     0x6c848410U,     0x33809a3fU,     0xbc4dfc35U, \
    0x54606ed5U,     0x6d7ecf50U,     0xfa049780U,     0xfd200b8fU, \
    0xa12b98bdU,     0xfd59bd37U,     0xa145ec4aU,     0xc9178b09U, \
    0xb7331472U,     0x5d125e05U,     0xce1d5ae2U,     0xe1f65421U, \
    0xaa55d5eaU,     0x094deb27U,     0x824019dfU,     0x1789662eU, \
    0xb36ed965U,     0x8d4e38d6U,     0x74d61661U,     0x5f16ded4U, \
    0x42d51951U,     0xc8d283b8U,     0x69a94bdeU,     0x028db697U, \
    0x01000103U,     0x30bf81a3U,     0x0c30bc81U,     0x1d550306U, \
    0x30050413U,     0xff010103U,     0x09061230U,     0x0401062bU, \
    0x01268201U,     0x30050403U,     0x01010203U,     0x09065f30U, \
    0x0401062bU,     0x01268201U,     0x30520422U,     0x60090650U, \
    0x65014886U,     0x03020403U,     0x53804004U,     0xdd70616cU, \
    0x08dc2686U,     0xd348f11aU,     0x5dfdc29eU,     0x78c50c09U, \
    0xe74766a7U,     0x4bd33f90U,     0x33432ed0U,     0x0e7be5ecU, \
    0x6f11ff24U,     0x6f9b4243U,     0x4b8341f5U,     0xc8f00ed4U, \
    0xce3a56d3U,     0xd20fedf5U,     0x0102b854U,     0x06183002U, \
    0x01062b09U,     0x26820104U,     0x0b042301U,     0x04040930U, \
    0x00000000U,     0x30020102U,     0x5503061dU,     0x16040e1dU, \
    0xdf041404U,     0x1484b3bbU,     0xcb0c2577U,     0x7da4332eU, \
    0x1259854dU,     0x0d30e5fbU,     0x862a0906U,     0x0df78648U, \
    0x050d0101U,     0x02820300U,     0x8c4b0001U,     0x98203277U, \
    0xcedec35fU,     0xc461782bU,     0x9813c794U,     0x61a2d240U, \
    0xec7cd6fdU,     0x4d22549aU,     0xf9ae28fbU,     0xc064651dU, \
    0xeb5863d4U,     0xadc7c15cU,     0x624aed5aU,     0xc33df586U, \
    0x1ec19667U,     0x7317f276U,     0x63715c3aU,     0x43ebc15dU, \
    0xc0809ab6U,     0x12fe75f0U,     0x06b47246U,     0xa3b4b488U, \
    0x72197801U,     0xadc52a87U,     0x4d0449f4U,     0x4f55adb3U, \
    0xa29c7d13U,     0x529683f4U,     0x23af7b45U,     0x753dbbcaU, \
    0xa532b24dU,     0x9712e2d3U,     0xb788d90dU,     0x488538f7U, \
    0xca4451e5U,     0x5fbb6ecfU,     0x2093394bU,     0x8a86f3c4U, \
    0x40ae743dU,     0xa6449069U,     0x66f80bc2U,     0xb3ccebc3U, \
    0x6ff130feU,     0x3053c1c9U,     0xc8477d73U,     0xfe379c73U, \
    0x37bec5a3U,     0x9e560893U,     0x6d610650U,     0x09247b31U, \
    0xae843c28U,     0x856f1fb5U,     0x6fa94b1dU,     0x1a7f6fc7U, \
    0xdd4f8b37U,     0x386cd2deU,     0x15c16d14U,     0xe08951f9U, \
    0x349a02f5U,     0x72f2bc43U,     0xfa16590bU,     0x2fe19743U, \
    0x8a82bd82U,     0x2e142dafU,     0xb6aafc43U,     0xcd78d2f1U, \
    0x9703d7ebU,     0x04d87979U,     0x09144804U,     0xa33603adU, \
    0x6e291337U,     0xb4ef4533U,     0xbaee04e7U,     0xb5279f7eU, \
    0x48e040f9U,     0xe86ac493U,     0x417cf558U,     0xc552cf18U, \
    0xa4de0b15U,     0x2d495680U,     0xe2db268cU,     0xef0f1d34U, \
    0x120725ecU,     0x4b613e59U,     0x13d97e49U,     0x627086e1U, \
    0xfc82a4c1U,     0xd99e5649U,     0xceb9561dU,     0x6a2f5b45U, \
    0xa2b7f68fU,     0x2486d663U,     0x3b317bb1U,     0x2fa03ffdU, \
    0xc71ab15dU,     0x8b20336cU,     0xd04d85edU,     0x258a16a7U, \
    0x49b21052U,     0xb798251dU,     0x96f253b1U,     0xd9f529d6U, \
    0x9558a2cfU,     0x1a453334U,     0x560a10f1U,     0xf1d3169eU, \
    0xbd7a82aaU,     0xb2ad2722U,     0x2cf0d048U,     0xc584628bU, \
    0x96f8bfd6U,     0x579dc9a5U,     0x613f3b82U,     0x79ab7ad2U, \
    0x77da0b19U,     0x9f0bc43cU,     0xa1cc9f4fU,     0x1f426141U, \
    0x303f8e5bU,     0xd6b4b233U,     0x488c62b5U,     0x7901cabaU, \
    0x24a6be1fU,     0x4f88b668U,     0xa9c460caU,     0x84e53675U, \
    0xff89cac8U,     0x5cca85cbU,     0x010081d7U\
} /* 1644 bytes */

#endif
