/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
*  \file sciclient_defaultBoardcfg_rm_hexhs.h
*
*  \brief File containing the Binary in a C array.
*
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#ifndef SCICLIENT_DEFAULTBOARDCFG_RM_HEXHS_H_
#define SCICLIENT_DEFAULTBOARDCFG_RM_HEXHS_H_


#define SCICLIENT_BOARDCFG_RM_SIZE_IN_BYTES (4249U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define SCICLIENT_BOARDCFG_RM { \
    0x67068230U,     0x4f048230U,     0x010203a0U,     0x2e140202U, \
    0x5cc61480U,     0x297b2dbcU,     0x7bd6dee5U,     0x9a9a84c1U, \
    0x30e0b05bU,     0x2a09060dU,     0xf7864886U,     0x0d01010dU, \
    0x81300005U,     0x300b318bU,     0x55030609U,     0x02130604U, \
    0x0b315355U,     0x03060930U,     0x0c080455U,     0x31435302U, \
    0x060d300fU,     0x07045503U,     0x6144060cU,     0x73616c6cU, \
    0x1f302131U,     0x04550306U,     0x54180c0aU,     0x73617865U, \
    0x736e4920U,     0x6d757274U,     0x73746e65U,     0x49202c2eU, \
    0x312e636eU,     0x060a300cU,     0x0b045503U,     0x4250030cU, \
    0x300f3155U,     0x5503060dU,     0x060c0304U,     0x65626c41U, \
    0x1c317472U,     0x09061a30U,     0x8648862aU,     0x09010df7U, \
    0x410d1601U,     0x7265626cU,     0x69744074U,     0x6d6f632eU, \
    0x0d171e30U,     0x33303532U,     0x34303430U,     0x39353434U, \
    0x320d175aU,     0x30343035U,     0x34343033U,     0x5a393534U, \
    0x318b8130U,     0x0609300bU,     0x06045503U,     0x53550213U, \
    0x09300b31U,     0x04550306U,     0x53020c08U,     0x300f3143U, \
    0x5503060dU,     0x060c0704U,     0x6c6c6144U,     0x21317361U, \
    0x03061f30U,     0x0c0a0455U,     0x78655418U,     0x49207361U, \
    0x7274736eU,     0x6e656d75U,     0x2c2e7374U,     0x636e4920U, \
    0x300c312eU,     0x5503060aU,     0x030c0b04U,     0x31554250U, \
    0x060d300fU,     0x03045503U,     0x6c41060cU,     0x74726562U, \
    0x1a301c31U,     0x862a0906U,     0x0df78648U,     0x16010901U, \
    0x626c410dU,     0x40747265U,     0x632e6974U,     0x82306d6fU, \
    0x0d302202U,     0x862a0906U,     0x0df78648U,     0x05010101U, \
    0x02820300U,     0x8230000fU,     0x82020a02U,     0xbf000102U, \
    0xd849ae14U,     0x6bd3727fU,     0x48ebcd23U,     0x22dc650eU, \
    0x4f0ef24dU,     0xb5edf682U,     0x7cdbddf2U,     0x596efa91U, \
    0xb6f7d5ffU,     0x8a1d04deU,     0xd995d2ccU,     0xc1c4e0d1U, \
    0xffbf50f8U,     0x22910c48U,     0x7b4c9a50U,     0x0a96f38bU, \
    0xa4b32628U,     0x55a9e0d9U,     0x3efb1a41U,     0xbf6c275bU, \
    0xaf71c0caU,     0xee22722fU,     0x62250146U,     0x04c73eadU, \
    0xb618b1f6U,     0x6e12c02cU,     0x3e9be20fU,     0xa8a0a6e5U, \
    0x41034506U,     0x1f164e17U,     0x84d674a9U,     0xa779d64eU, \
    0xa911b810U,     0x251f920eU,     0xf2b17fddU,     0x68f2b9d1U, \
    0x4b5933d8U,     0xcc777d82U,     0x23fa9cd1U,     0x8858fbb4U, \
    0xd5eacdf2U,     0x752cf216U,     0xc362fa2dU,     0xe06e09c1U, \
    0xb5e07006U,     0x62990907U,     0xe7e4d6d9U,     0x82c86d6cU, \
    0xf7935007U,     0xd1edd8e2U,     0x9ed0e35fU,     0xd95493cfU, \
    0xce5ddd5fU,     0xabf16037U,     0x7b048a14U,     0x7fbaa765U, \
    0x7c4545dfU,     0xae5ba14bU,     0x3d94c64eU,     0xd2874e8cU, \
    0xf3a43c94U,     0xf2fcda9fU,     0x0de77c36U,     0x37425aadU, \
    0xd0812af1U,     0x67a7a16eU,     0xed871e03U,     0x4a73bb00U, \
    0xa2312868U,     0x04a39a82U,     0xff87e8c1U,     0xc1aa7e45U, \
    0x053bd49fU,     0x21fd83c7U,     0x7fbdfe71U,     0x1916c938U, \
    0x03e60e52U,     0x1e1d8d33U,     0xcd1c36c9U,     0x29829d4eU, \
    0x2a9bcd88U,     0x7b5f6cbeU,     0x793ab2b2U,     0xf57d6a00U, \
    0x1e9d1aadU,     0xcf2a58cdU,     0x804ef45eU,     0xdd4f3babU, \
    0x34ded4f8U,     0xd920c4a2U,     0x852d1959U,     0x681f5e02U, \
    0xb98d4cb1U,     0x2de90611U,     0x8c58b576U,     0x6e37a850U, \
    0x836f7866U,     0x344d4630U,     0x4a18b49fU,     0x7bfabbb9U, \
    0x32d6aec5U,     0x6c848410U,     0x33809a3fU,     0xbc4dfc35U, \
    0x54606ed5U,     0x6d7ecf50U,     0xfa049780U,     0xfd200b8fU, \
    0xa12b98bdU,     0xfd59bd37U,     0xa145ec4aU,     0xc9178b09U, \
    0xb7331472U,     0x5d125e05U,     0xce1d5ae2U,     0xe1f65421U, \
    0xaa55d5eaU,     0x094deb27U,     0x824019dfU,     0x1789662eU, \
    0xb36ed965U,     0x8d4e38d6U,     0x74d61661U,     0x5f16ded4U, \
    0x42d51951U,     0xc8d283b8U,     0x69a94bdeU,     0x028db697U, \
    0x01000103U,     0x30c081a3U,     0x0c30bd81U,     0x1d550306U, \
    0x30050413U,     0xff010103U,     0x09061230U,     0x0401062bU, \
    0x01268201U,     0x30050403U,     0x01010203U,     0x09066030U, \
    0x0401062bU,     0x01268201U,     0x30530422U,     0x60090651U, \
    0x65014886U,     0x03020403U,     0x2f224004U,     0x02c9638eU, \
    0x8b74cb0eU,     0xc6ea951bU,     0x15c3559bU,     0x3d5e90c5U, \
    0xf64a6e4bU,     0x678e1020U,     0x944117d4U,     0xbec5ff9eU, \
    0xdb0c0010U,     0xeccc23d0U,     0x9a3ec711U,     0x5a7f1df1U, \
    0x54297e53U,     0xf2475583U,     0x0202ff6fU,     0x18302e0aU, \
    0x062b0906U,     0x82010401U,     0x04230126U,     0x0409300bU, \
    0x00000004U,     0x02010200U,     0x03061d30U,     0x040e1d55U, \
    0x04140416U,     0x84b3bbdfU,     0x0c257714U,     0xa4332ecbU, \
    0x59854d7dU,     0x30e5fb12U,     0x2a09060dU,     0xf7864886U, \
    0x0d01010dU,     0x82030005U,     0x58000102U,     0xb2c48e35U, \
    0xb5f2c8f5U,     0xa8c1e811U,     0xb1af2c3eU,     0x12394ccaU, \
    0x67e71ef4U,     0x69a7045eU,     0xfaeb5ff7U,     0xf0d7f41eU, \
    0xd47ecadcU,     0x6204ffc2U,     0x7ca3a118U,     0x72658564U, \
    0x7e91a8bfU,     0x00495461U,     0xdb8f0397U,     0x11d5b434U, \
    0x438147d4U,     0xc0948742U,     0x62dcffd2U,     0x02b3081dU, \
    0x316a76f4U,     0x4ad9f542U,     0x24952f94U,     0x9f21c909U, \
    0x97b85ad3U,     0x31740cffU,     0xf5ce9752U,     0xd1041b2dU, \
    0x7f1c69b7U,     0xf04c6f14U,     0xbe0da899U,     0x37928cedU, \
    0xa49818bbU,     0x2f4f4ba0U,     0x0f08eff8U,     0x8fb6f08cU, \
    0x9faff683U,     0x5f928e01U,     0x321a32f3U,     0x57ce0d9bU, \
    0x81755254U,     0x001f4079U,     0x4f659dadU,     0xb9e08a5bU, \
    0xd78604cbU,     0x1cafb023U,     0xd0723d6aU,     0x62a7f698U, \
    0x04b85f01U,     0xbafa915bU,     0x956ebef3U,     0x25fad63cU, \
    0x6c1ef01eU,     0xf1e34698U,     0x7617b700U,     0x95399d82U, \
    0x3974fea9U,     0xa971ac03U,     0xc4f01086U,     0x0ec2b0aaU, \
    0x5abd7993U,     0xa333ef59U,     0x193480d8U,     0x2be5647cU, \
    0x80e125a7U,     0x41276ed1U,     0x1c66f439U,     0x0dc7a1c3U, \
    0x16aa4df4U,     0x190004bdU,     0x9e8f9236U,     0xa0f69c9bU, \
    0x766dad9cU,     0xf97b9563U,     0x700b0898U,     0xc865cce2U, \
    0xb3f63606U,     0x456fbe67U,     0xb70700f0U,     0xbb087a03U, \
    0xad27f67eU,     0x2863dfb2U,     0x5a7996dcU,     0xa74bd8daU, \
    0x2fb0c71fU,     0xa2c17dd7U,     0x1a0ae655U,     0x16acfe63U, \
    0x4b888461U,     0x4fb6b58dU,     0xbde18ed6U,     0xf8a36f9dU, \
    0x26ef99b9U,     0x9d2ce841U,     0xce72a657U,     0xeb05de53U, \
    0x8756b3a0U,     0x114765c4U,     0x940a7f69U,     0x92d74996U, \
    0x1c201694U,     0xdb916fa6U,     0x2ade2ef1U,     0x7c900b66U, \
    0xd6a9c8c0U,     0xc5219d87U,     0x1053fad5U,     0xc55fd486U, \
    0x96f9cc94U,     0x95f23c0aU,     0xb75315c4U,     0xe2816a74U, \
    0x9b428aeaU,     0x1be48c1cU,     0x4357f7ecU,     0x96911f54U, \
    0x076aa043U,     0x1ec3c15aU,     0xd00bf4c7U,     0x27c528b9U, \
    0x58b780bfU,     0x4b34ba60U,     0x45df535cU,     0x93e48000U, \
    0x16152fceU,     0x39cc6a7eU,     0x00a60db9U,     0x644c4101U, \
    0xaa2a0301U,     0xaaaaaaaaU,     0xaaaaaaaaU,     0xaaaa2a05U, \
    0xaaaaaaaaU,     0x0caaaaaaU,     0xaaaaaa2aU,     0xaaaaaaaaU, \
    0x2a0daaaaU,     0xaaaaaaaaU,     0xaaaaaaaaU,     0xaa2a23aaU, \
    0xaaaaaaaaU,     0xaaaaaaaaU,     0xaaaa2a25U,     0xaaaaaaaaU, \
    0x00aaaaaaU,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x087b2500U,     0x00080000U,     0x20000000U,     0x03200000U, \
    0x20002000U,     0x05200000U,     0x18000000U,     0x03208000U, \
    0x18001800U,     0x05208000U,     0x08000000U,     0x0320c000U, \
    0x08000800U,     0x0520c000U,     0x08001000U,     0x2320c000U, \
    0x08001800U,     0x2520c000U,     0x10002000U,     0x0c20c000U, \
    0x10003000U,     0x0d20c000U,     0x30000000U,     0x80220000U, \
    0x08000000U,     0x03224000U,     0x08000800U,     0x05224000U, \
    0x08001000U,     0x0c224000U,     0x08001800U,     0x0d224000U, \
    0x40000000U,     0x8033ca00U,     0x00500000U,     0x8033cd04U, \
    0x40000000U,     0x80340a00U,     0x00580000U,     0x80340d04U, \
    0x56001200U,     0x0c344a00U,     0x20006800U,     0x0d344a00U, \
    0x10008800U,     0x03344a00U,     0x10009800U,     0x05344a00U, \
    0x2000a800U,     0x23344a00U,     0x1800c800U,     0x25344a00U, \
    0x2000e000U,     0x80344a00U,     0x00001200U,     0x0c344d04U, \
    0x00041200U,     0x0d344d02U,     0x80061200U,     0x03344d00U, \
    0x80069200U,     0x05344d00U,     0x00071200U,     0x23344d01U, \
    0x00081200U,     0x25344d02U,     0xee0a1200U,     0x80344d07U, \
    0x04000000U,     0x0c348000U,     0x04000400U,     0x0d348000U, \
    0x04000800U,     0x03348000U,     0x04000c00U,     0x05348000U, \
    0x10001000U,     0x23348000U,     0x10002000U,     0x25348000U, \
    0x10003000U,     0x80348000U,     0x01000000U,     0x8034c000U, \
    0xc8007800U,     0x0c34c100U,     0x28014000U,     0x0d34c100U, \
    0x20016800U,     0x0334c100U,     0x20018800U,     0x0534c100U, \
    0x0001a800U,     0x2334c101U,     0x0002a800U,     0x2534c101U, \
    0x2603a800U,     0x8034c100U,     0x04004000U,     0x0c34c200U, \
    0x02004400U,     0x0d34c200U,     0x02004600U,     0x0334c200U, \
    0x02004800U,     0x0534c200U,     0x02004a00U,     0x2334c200U, \
    0x02004c00U,     0x2534c200U,     0x14004e00U,     0x0c34c200U, \
    0x04006200U,     0x0d34c200U,     0x08006600U,     0x2334c200U, \
    0x08006e00U,     0x2534c200U,     0x02007600U,     0x8034c200U, \
    0x04000400U,     0x0c34c300U,     0x02000800U,     0x0d34c300U, \
    0x02000a00U,     0x0334c300U,     0x02000c00U,     0x0534c300U, \
    0x02000e00U,     0x2334c300U,     0x02001000U,     0x2534c300U, \
    0x14001200U,     0x0c34c300U,     0x04002600U,     0x0d34c300U, \
    0x08002a00U,     0x2334c300U,     0x08003200U,     0x2534c300U, \
    0x02003a00U,     0x8034c300U,     0x00003e00U,     0x0c34c500U, \
    0x01003e00U,     0x0c34c500U,     0x00003e00U,     0x2334c500U, \
    0x01003f00U,     0x2334c500U,     0x00003c00U,     0x2534c600U, \
    0x02003c00U,     0x2534c600U,     0x00000200U,     0x0c34c700U, \
    0x01000200U,     0x0c34c700U,     0x00000200U,     0x2334c700U, \
    0x01000300U,     0x2334c700U,     0x00000000U,     0x2534c800U, \
    0x02000000U,     0x2534c800U,     0x01000200U,     0x0c34ca00U, \
    0x01000300U,     0x0d34ca00U,     0x03000000U,     0x0c34cb00U, \
    0x02000300U,     0x0d34cb00U,     0x01000500U,     0x0334cb00U, \
    0x01000600U,     0x0534cb00U,     0x10000700U,     0x2334cb00U, \
    0x08001700U,     0x2534cb00U,     0x01001f00U,     0x8034cb00U, \
    0x08003c00U,     0x0c350000U,     0x08004400U,     0x0d350000U, \
    0x08004c00U,     0x25350000U,     0x42005400U,     0x80350000U, \
    0x01000000U,     0x80350100U,     0x00c00000U,     0x80350204U, \
    0x01000000U,     0x80350300U,     0x04000400U,     0x0c350a00U, \
    0x02000800U,     0x0d350a00U,     0x02000a00U,     0x03350a00U, \
    0x02000c00U,     0x05350a00U,     0x02000e00U,     0x23350a00U, \
    0x02001000U,     0x25350a00U,     0x14001200U,     0x0c350a00U, \
    0x04002600U,     0x0d350a00U,     0x08002a00U,     0x23350a00U, \
    0x08003200U,     0x25350a00U,     0x02003a00U,     0x80350a00U, \
    0x00000200U,     0x0c350b00U,     0x01000200U,     0x0c350b00U, \
    0x00000200U,     0x23350b00U,     0x01000300U,     0x23350b00U, \
    0x00000000U,     0x25350c00U,     0x02000000U,     0x25350c00U, \
    0x04000400U,     0x0c350d00U,     0x02000800U,     0x0d350d00U, \
    0x02000a00U,     0x03350d00U,     0x02000c00U,     0x05350d00U, \
    0x02000e00U,     0x23350d00U,     0x02001000U,     0x25350d00U, \
    0x14001200U,     0x0c350d00U,     0x04002600U,     0x0d350d00U, \
    0x08002a00U,     0x23350d00U,     0x08003200U,     0x25350d00U, \
    0x02003a00U,     0x80350d00U,     0x00000200U,     0x0c350f00U, \
    0x01000200U,     0x0c350f00U,     0x00000200U,     0x23350f00U, \
    0x01000300U,     0x23350f00U,     0x00000000U,     0x25351000U, \
    0x02000000U,     0x25351000U,     0x80000a00U,     0x0c354000U, \
    0x36008a00U,     0x0d354000U,     0x1c00c400U,     0x23354000U, \
    0x1c00e400U,     0x25354000U,     0x04019000U,     0x03354000U, \
    0x04019400U,     0x05354000U,     0x20000f00U,     0x0c3a4a00U, \
    0x10002f00U,     0x0d3a4a00U,     0x40003f00U,     0x033a4a00U, \
    0x20007f00U,     0x053a4a00U,     0x10009f00U,     0x233a4a00U, \
    0x1000af00U,     0x253a4a00U,     0x4100bf00U,     0x803a4a00U, \
    0x80400f00U,     0x0c3a4d00U,     0x80408f00U,     0x0d3a4d00U, \
    0x00410f00U,     0x033a4d01U,     0x80420f00U,     0x053a4d00U, \
    0x80428f00U,     0x233a4d00U,     0x80430f00U,     0x253a4d00U, \
    0x71438f00U,     0x803a4d02U,     0x08000100U,     0x0c3a8000U, \
    0x04000900U,     0x0d3a8000U,     0x10000d00U,     0x033a8000U, \
    0x10001d00U,     0x053a8000U,     0x08002d00U,     0x233a8000U, \
    0x08003500U,     0x253a8000U,     0x03003d00U,     0x803a8000U, \
    0x01000000U,     0x803ac000U,     0x20006000U,     0x0c3ac100U, \
    0x10008000U,     0x0d3ac100U,     0x20009000U,     0x033ac100U, \
    0x2000b000U,     0x053ac100U,     0x1000d000U,     0x233ac100U, \
    0x1000e000U,     0x253ac100U,     0x0c00f000U,     0x803ac100U, \
    0x03003200U,     0x0c3ac200U,     0x02003500U,     0x0d3ac200U, \
    0x02003700U,     0x033ac200U,     0x02003900U,     0x053ac200U, \
    0x02003b00U,     0x233ac200U,     0x02003d00U,     0x253ac200U, \
    0x09003f00U,     0x0c3ac200U,     0x04004800U,     0x0d3ac200U, \
    0x04004c00U,     0x033ac200U,     0x04005000U,     0x053ac200U, \
    0x04005400U,     0x233ac200U,     0x04005800U,     0x253ac200U, \
    0x01005c00U,     0x803ac200U,     0x03000200U,     0x0c3ac300U, \
    0x02000500U,     0x0d3ac300U,     0x02000700U,     0x033ac300U, \
    0x02000900U,     0x053ac300U,     0x02000b00U,     0x233ac300U, \
    0x02000d00U,     0x253ac300U,     0x09000f00U,     0x0c3ac300U, \
    0x04001800U,     0x0d3ac300U,     0x04001c00U,     0x033ac300U, \
    0x04002000U,     0x053ac300U,     0x04002400U,     0x233ac300U, \
    0x04002800U,     0x253ac300U,     0x02002c00U,     0x803ac300U, \
    0x00003000U,     0x033ac500U,     0x00003000U,     0x0c3ac500U, \
    0x01003000U,     0x0c3ac500U,     0x01003100U,     0x033ac500U, \
    0x00000000U,     0x033ac700U,     0x00000000U,     0x0c3ac700U, \
    0x01000000U,     0x0c3ac700U,     0x01000100U,     0x033ac700U, \
    0x01000200U,     0x0c3aca00U,     0x01000300U,     0x0d3aca00U, \
    0x03000000U,     0x0c3acb00U,     0x02000300U,     0x0d3acb00U, \
    0x06000500U,     0x033acb00U,     0x06000b00U,     0x053acb00U, \
    0x05001100U,     0x233acb00U,     0x05001600U,     0x253acb00U, \
    0x05001b00U,     0x803acb00U,     0x08003000U,     0x0c3b0000U, \
    0x04003800U,     0x0d3b0000U,     0x08003c00U,     0x033b0000U, \
    0x04004400U,     0x053b0000U,     0x08004800U,     0x233b0000U, \
    0x04005000U,     0x253b0000U,     0x0c005400U,     0x803b0000U, \
    0x01000000U,     0x803b0100U,     0x00dc0000U,     0x803b0201U, \
    0x01000000U,     0x803b0300U,     0x03000200U,     0x0c3b0a00U, \
    0x02000500U,     0x0d3b0a00U,     0x02000700U,     0x033b0a00U, \
    0x02000900U,     0x053b0a00U,     0x02000b00U,     0x233b0a00U, \
    0x02000d00U,     0x253b0a00U,     0x09000f00U,     0x0c3b0a00U, \
    0x04001800U,     0x0d3b0a00U,     0x04001c00U,     0x033b0a00U, \
    0x04002000U,     0x053b0a00U,     0x04002400U,     0x233b0a00U, \
    0x04002800U,     0x253b0a00U,     0x01002c00U,     0x803b0a00U, \
    0x00000000U,     0x033b0b00U,     0x00000000U,     0x0c3b0b00U, \
    0x01000000U,     0x0c3b0b00U,     0x01000100U,     0x033b0b00U, \
    0x03000200U,     0x0c3b0d00U,     0x02000500U,     0x0d3b0d00U, \
    0x02000700U,     0x033b0d00U,     0x02000900U,     0x053b0d00U, \
    0x02000b00U,     0x233b0d00U,     0x02000d00U,     0x253b0d00U, \
    0x09000f00U,     0x0c3b0d00U,     0x04001800U,     0x0d3b0d00U, \
    0x04001c00U,     0x033b0d00U,     0x04002000U,     0x053b0d00U, \
    0x04002400U,     0x233b0d00U,     0x04002800U,     0x253b0d00U, \
    0x02002c00U,     0x803b0d00U,     0x00000000U,     0x033b0f00U, \
    0x00000000U,     0x0c3b0f00U,     0x01000000U,     0x0c3b0f00U, \
    0x01000100U,     0x033b0f00U,     0x14000b00U,     0x033b4000U, \
    0x1c002400U,     0x053b4000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U\
} /* 4249 bytes */

#endif
