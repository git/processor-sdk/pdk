/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
*  \file sciclient_defaultBoardcfg_security_hexhs.h
*
*  \brief File containing the Binary in a C array.
*
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#ifndef SCICLIENT_DEFAULTBOARDCFG_SECURITY_HEXHS_H_
#define SCICLIENT_DEFAULTBOARDCFG_SECURITY_HEXHS_H_


#define SCICLIENT_BOARDCFG_SECURITY_SIZE_IN_BYTES (1992U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define SCICLIENT_BOARDCFG_SECURITY { \
    0x67068230U,     0x4f048230U,     0x010203a0U,     0x23140202U, \
    0x8b52721bU,     0x5ce1a32dU,     0xc2e54f68U,     0x4512140aU, \
    0x30ed75c5U,     0x2a09060dU,     0xf7864886U,     0x0d01010dU, \
    0x81300005U,     0x300b318bU,     0x55030609U,     0x02130604U, \
    0x0b315355U,     0x03060930U,     0x0c080455U,     0x31435302U, \
    0x060d300fU,     0x07045503U,     0x6144060cU,     0x73616c6cU, \
    0x1f302131U,     0x04550306U,     0x54180c0aU,     0x73617865U, \
    0x736e4920U,     0x6d757274U,     0x73746e65U,     0x49202c2eU, \
    0x312e636eU,     0x060a300cU,     0x0b045503U,     0x4250030cU, \
    0x300f3155U,     0x5503060dU,     0x060c0304U,     0x65626c41U, \
    0x1c317472U,     0x09061a30U,     0x8648862aU,     0x09010df7U, \
    0x410d1601U,     0x7265626cU,     0x69744074U,     0x6d6f632eU, \
    0x0d171e30U,     0x33303532U,     0x34303430U,     0x39353434U, \
    0x320d175aU,     0x30343035U,     0x34343033U,     0x5a393534U, \
    0x318b8130U,     0x0609300bU,     0x06045503U,     0x53550213U, \
    0x09300b31U,     0x04550306U,     0x53020c08U,     0x300f3143U, \
    0x5503060dU,     0x060c0704U,     0x6c6c6144U,     0x21317361U, \
    0x03061f30U,     0x0c0a0455U,     0x78655418U,     0x49207361U, \
    0x7274736eU,     0x6e656d75U,     0x2c2e7374U,     0x636e4920U, \
    0x300c312eU,     0x5503060aU,     0x030c0b04U,     0x31554250U, \
    0x060d300fU,     0x03045503U,     0x6c41060cU,     0x74726562U, \
    0x1a301c31U,     0x862a0906U,     0x0df78648U,     0x16010901U, \
    0x626c410dU,     0x40747265U,     0x632e6974U,     0x82306d6fU, \
    0x0d302202U,     0x862a0906U,     0x0df78648U,     0x05010101U, \
    0x02820300U,     0x8230000fU,     0x82020a02U,     0xbf000102U, \
    0xd849ae14U,     0x6bd3727fU,     0x48ebcd23U,     0x22dc650eU, \
    0x4f0ef24dU,     0xb5edf682U,     0x7cdbddf2U,     0x596efa91U, \
    0xb6f7d5ffU,     0x8a1d04deU,     0xd995d2ccU,     0xc1c4e0d1U, \
    0xffbf50f8U,     0x22910c48U,     0x7b4c9a50U,     0x0a96f38bU, \
    0xa4b32628U,     0x55a9e0d9U,     0x3efb1a41U,     0xbf6c275bU, \
    0xaf71c0caU,     0xee22722fU,     0x62250146U,     0x04c73eadU, \
    0xb618b1f6U,     0x6e12c02cU,     0x3e9be20fU,     0xa8a0a6e5U, \
    0x41034506U,     0x1f164e17U,     0x84d674a9U,     0xa779d64eU, \
    0xa911b810U,     0x251f920eU,     0xf2b17fddU,     0x68f2b9d1U, \
    0x4b5933d8U,     0xcc777d82U,     0x23fa9cd1U,     0x8858fbb4U, \
    0xd5eacdf2U,     0x752cf216U,     0xc362fa2dU,     0xe06e09c1U, \
    0xb5e07006U,     0x62990907U,     0xe7e4d6d9U,     0x82c86d6cU, \
    0xf7935007U,     0xd1edd8e2U,     0x9ed0e35fU,     0xd95493cfU, \
    0xce5ddd5fU,     0xabf16037U,     0x7b048a14U,     0x7fbaa765U, \
    0x7c4545dfU,     0xae5ba14bU,     0x3d94c64eU,     0xd2874e8cU, \
    0xf3a43c94U,     0xf2fcda9fU,     0x0de77c36U,     0x37425aadU, \
    0xd0812af1U,     0x67a7a16eU,     0xed871e03U,     0x4a73bb00U, \
    0xa2312868U,     0x04a39a82U,     0xff87e8c1U,     0xc1aa7e45U, \
    0x053bd49fU,     0x21fd83c7U,     0x7fbdfe71U,     0x1916c938U, \
    0x03e60e52U,     0x1e1d8d33U,     0xcd1c36c9U,     0x29829d4eU, \
    0x2a9bcd88U,     0x7b5f6cbeU,     0x793ab2b2U,     0xf57d6a00U, \
    0x1e9d1aadU,     0xcf2a58cdU,     0x804ef45eU,     0xdd4f3babU, \
    0x34ded4f8U,     0xd920c4a2U,     0x852d1959U,     0x681f5e02U, \
    0xb98d4cb1U,     0x2de90611U,     0x8c58b576U,     0x6e37a850U, \
    0x836f7866U,     0x344d4630U,     0x4a18b49fU,     0x7bfabbb9U, \
    0x32d6aec5U,     0x6c848410U,     0x33809a3fU,     0xbc4dfc35U, \
    0x54606ed5U,     0x6d7ecf50U,     0xfa049780U,     0xfd200b8fU, \
    0xa12b98bdU,     0xfd59bd37U,     0xa145ec4aU,     0xc9178b09U, \
    0xb7331472U,     0x5d125e05U,     0xce1d5ae2U,     0xe1f65421U, \
    0xaa55d5eaU,     0x094deb27U,     0x824019dfU,     0x1789662eU, \
    0xb36ed965U,     0x8d4e38d6U,     0x74d61661U,     0x5f16ded4U, \
    0x42d51951U,     0xc8d283b8U,     0x69a94bdeU,     0x028db697U, \
    0x01000103U,     0x30c081a3U,     0x0c30bd81U,     0x1d550306U, \
    0x30050413U,     0xff010103U,     0x09061230U,     0x0401062bU, \
    0x01268201U,     0x30050403U,     0x01010203U,     0x09066030U, \
    0x0401062bU,     0x01268201U,     0x30530422U,     0x60090651U, \
    0x65014886U,     0x03020403U,     0x22134004U,     0xc7049072U, \
    0xd48b8448U,     0x855dcb78U,     0x7891849eU,     0xfa1211f1U, \
    0xc4079793U,     0xe65de268U,     0x04f65868U,     0x15887135U, \
    0x6f739c52U,     0x393116b0U,     0xe7494cc0U,     0x178e2a92U, \
    0x41c5c816U,     0x746c85eaU,     0x0202e1f8U,     0x18305d01U, \
    0x062b0906U,     0x82010401U,     0x04230126U,     0x0409300bU, \
    0x00000004U,     0x02010200U,     0x03061d30U,     0x040e1d55U, \
    0x04140416U,     0x84b3bbdfU,     0x0c257714U,     0xa4332ecbU, \
    0x59854d7dU,     0x30e5fb12U,     0x2a09060dU,     0xf7864886U, \
    0x0d01010dU,     0x82030005U,     0x6e000102U,     0xb1d740d2U, \
    0xbbd9ff81U,     0x5bd54a8aU,     0x4e58d66fU,     0x38fb1685U, \
    0x18abf846U,     0xccb6af40U,     0x5a7bdbacU,     0xf9da64beU, \
    0xf1e6a887U,     0x786497b5U,     0x380bbac5U,     0xdbddb96aU, \
    0xf2523617U,     0xc66b9bb1U,     0x9c6c4f0eU,     0xded69e02U, \
    0xee2b829eU,     0xaeab890cU,     0x470e9ff9U,     0x27fc8188U, \
    0x3d0a2c4cU,     0x82fc7444U,     0xf66da823U,     0x0bddda98U, \
    0x5de71fdaU,     0x21c6386aU,     0x0b1f006aU,     0xf160edb9U, \
    0x86bf8d3bU,     0x0bcecb21U,     0x84cd32ccU,     0x8b4664fbU, \
    0x79d8f8d2U,     0x0933ae0aU,     0xefe9fa55U,     0x6018ed05U, \
    0x06330a22U,     0xdb9a1829U,     0x665501b7U,     0x765fd447U, \
    0x49d37b1dU,     0xe8a65b1bU,     0xe7312b77U,     0xc2316d96U, \
    0x9a768a5fU,     0xe0b36e78U,     0x3e00216eU,     0xf272b843U, \
    0x32162488U,     0xe47457daU,     0x09432cecU,     0x0c85cd2fU, \
    0xe6ae3d05U,     0x3e52bcacU,     0x7917a278U,     0x0846e336U, \
    0x5b47fe90U,     0x9d2b2179U,     0x2ce5389aU,     0x15630066U, \
    0xe49b4f1fU,     0x58f8be6bU,     0x05e57c4bU,     0x749a6e7cU, \
    0x21b7ceebU,     0xee9b24e8U,     0x4d83abaeU,     0xe2c6c305U, \
    0x19cf3fe8U,     0x258b3ea3U,     0xdff37b43U,     0x1f0ddd5eU, \
    0x35c3dd65U,     0x5ed59947U,     0xa94b03d9U,     0xd35fcc4bU, \
    0x51a906d4U,     0xfc8f927cU,     0xbb0cc12bU,     0x6589ce3fU, \
    0x460a14f9U,     0x42b772dcU,     0x7a6bc19aU,     0x1ec8b465U, \
    0xcdf0ac12U,     0x4ff5acf7U,     0xbb173d0eU,     0xe5c4b0c7U, \
    0xd781fa7bU,     0xb01b66bdU,     0xfa0e8f56U,     0x5ad5136eU, \
    0x08a09270U,     0x0b1fac5fU,     0x87dc30a5U,     0xdf17a2d8U, \
    0x4d7db257U,     0x62e61e47U,     0xdb8c2b7cU,     0x4da8fb0cU, \
    0xdb934057U,     0x79c891f0U,     0x9af273eeU,     0x4b1a9316U, \
    0x9b270a45U,     0xd7786eb2U,     0xc24830c6U,     0xc5d8039eU, \
    0xf2868283U,     0x993bebb8U,     0x818ddbe9U,     0x7fa0f4cbU, \
    0x1bf8cea9U,     0x3963c471U,     0x4b927bacU,     0x6091e4e2U, \
    0xed5b78c4U,     0x912dab35U,     0x98b26a31U,     0x416a3012U, \
    0x8c24b5d6U,     0xc11b85e3U,     0xdf9a27abU,     0x7c46ddc2U, \
    0x609a835bU,     0x5eb362e9U,     0x00d059ceU,     0xa4f1ea01U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x448d2700U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x45408100U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x51700000U,     0x0080000cU, \
    0x005a0000U,     0x23be0000U,     0x00000000U,     0x42af045aU, \
    0x00000010U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U\
} /* 1992 bytes */

#endif
