/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
*  \file sciclient_defaultBoardcfg_hexhs.h
*
*  \brief File containing the Binary in a C array.
*
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#ifndef SCICLIENT_DEFAULTBOARDCFG_HEXHS_H_
#define SCICLIENT_DEFAULTBOARDCFG_HEXHS_H_


#define SCICLIENT_BOARDCFG_SIZE_IN_BYTES (1671U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define SCICLIENT_BOARDCFG { \
    0x66068230U,     0x4e048230U,     0x010203a0U,     0x53140202U, \
    0xb6a798c1U,     0x0326560dU,     0x8950afc5U,     0x86893ed2U, \
    0x30284156U,     0x2a09060dU,     0xf7864886U,     0x0d01010dU, \
    0x81300005U,     0x300b318bU,     0x55030609U,     0x02130604U, \
    0x0b315355U,     0x03060930U,     0x0c080455U,     0x31435302U, \
    0x060d300fU,     0x07045503U,     0x6144060cU,     0x73616c6cU, \
    0x1f302131U,     0x04550306U,     0x54180c0aU,     0x73617865U, \
    0x736e4920U,     0x6d757274U,     0x73746e65U,     0x49202c2eU, \
    0x312e636eU,     0x060a300cU,     0x0b045503U,     0x4250030cU, \
    0x300f3155U,     0x5503060dU,     0x060c0304U,     0x65626c41U, \
    0x1c317472U,     0x09061a30U,     0x8648862aU,     0x09010df7U, \
    0x410d1601U,     0x7265626cU,     0x69744074U,     0x6d6f632eU, \
    0x0d171e30U,     0x33303532U,     0x34303430U,     0x39353434U, \
    0x320d175aU,     0x30343035U,     0x34343033U,     0x5a393534U, \
    0x318b8130U,     0x0609300bU,     0x06045503U,     0x53550213U, \
    0x09300b31U,     0x04550306U,     0x53020c08U,     0x300f3143U, \
    0x5503060dU,     0x060c0704U,     0x6c6c6144U,     0x21317361U, \
    0x03061f30U,     0x0c0a0455U,     0x78655418U,     0x49207361U, \
    0x7274736eU,     0x6e656d75U,     0x2c2e7374U,     0x636e4920U, \
    0x300c312eU,     0x5503060aU,     0x030c0b04U,     0x31554250U, \
    0x060d300fU,     0x03045503U,     0x6c41060cU,     0x74726562U, \
    0x1a301c31U,     0x862a0906U,     0x0df78648U,     0x16010901U, \
    0x626c410dU,     0x40747265U,     0x632e6974U,     0x82306d6fU, \
    0x0d302202U,     0x862a0906U,     0x0df78648U,     0x05010101U, \
    0x02820300U,     0x8230000fU,     0x82020a02U,     0xbf000102U, \
    0xd849ae14U,     0x6bd3727fU,     0x48ebcd23U,     0x22dc650eU, \
    0x4f0ef24dU,     0xb5edf682U,     0x7cdbddf2U,     0x596efa91U, \
    0xb6f7d5ffU,     0x8a1d04deU,     0xd995d2ccU,     0xc1c4e0d1U, \
    0xffbf50f8U,     0x22910c48U,     0x7b4c9a50U,     0x0a96f38bU, \
    0xa4b32628U,     0x55a9e0d9U,     0x3efb1a41U,     0xbf6c275bU, \
    0xaf71c0caU,     0xee22722fU,     0x62250146U,     0x04c73eadU, \
    0xb618b1f6U,     0x6e12c02cU,     0x3e9be20fU,     0xa8a0a6e5U, \
    0x41034506U,     0x1f164e17U,     0x84d674a9U,     0xa779d64eU, \
    0xa911b810U,     0x251f920eU,     0xf2b17fddU,     0x68f2b9d1U, \
    0x4b5933d8U,     0xcc777d82U,     0x23fa9cd1U,     0x8858fbb4U, \
    0xd5eacdf2U,     0x752cf216U,     0xc362fa2dU,     0xe06e09c1U, \
    0xb5e07006U,     0x62990907U,     0xe7e4d6d9U,     0x82c86d6cU, \
    0xf7935007U,     0xd1edd8e2U,     0x9ed0e35fU,     0xd95493cfU, \
    0xce5ddd5fU,     0xabf16037U,     0x7b048a14U,     0x7fbaa765U, \
    0x7c4545dfU,     0xae5ba14bU,     0x3d94c64eU,     0xd2874e8cU, \
    0xf3a43c94U,     0xf2fcda9fU,     0x0de77c36U,     0x37425aadU, \
    0xd0812af1U,     0x67a7a16eU,     0xed871e03U,     0x4a73bb00U, \
    0xa2312868U,     0x04a39a82U,     0xff87e8c1U,     0xc1aa7e45U, \
    0x053bd49fU,     0x21fd83c7U,     0x7fbdfe71U,     0x1916c938U, \
    0x03e60e52U,     0x1e1d8d33U,     0xcd1c36c9U,     0x29829d4eU, \
    0x2a9bcd88U,     0x7b5f6cbeU,     0x793ab2b2U,     0xf57d6a00U, \
    0x1e9d1aadU,     0xcf2a58cdU,     0x804ef45eU,     0xdd4f3babU, \
    0x34ded4f8U,     0xd920c4a2U,     0x852d1959U,     0x681f5e02U, \
    0xb98d4cb1U,     0x2de90611U,     0x8c58b576U,     0x6e37a850U, \
    0x836f7866U,     0x344d4630U,     0x4a18b49fU,     0x7bfabbb9U, \
    0x32d6aec5U,     0x6c848410U,     0x33809a3fU,     0xbc4dfc35U, \
    0x54606ed5U,     0x6d7ecf50U,     0xfa049780U,     0xfd200b8fU, \
    0xa12b98bdU,     0xfd59bd37U,     0xa145ec4aU,     0xc9178b09U, \
    0xb7331472U,     0x5d125e05U,     0xce1d5ae2U,     0xe1f65421U, \
    0xaa55d5eaU,     0x094deb27U,     0x824019dfU,     0x1789662eU, \
    0xb36ed965U,     0x8d4e38d6U,     0x74d61661U,     0x5f16ded4U, \
    0x42d51951U,     0xc8d283b8U,     0x69a94bdeU,     0x028db697U, \
    0x01000103U,     0x30bf81a3U,     0x0c30bc81U,     0x1d550306U, \
    0x30050413U,     0xff010103U,     0x09061230U,     0x0401062bU, \
    0x01268201U,     0x30050403U,     0x01010203U,     0x09065f30U, \
    0x0401062bU,     0x01268201U,     0x30520422U,     0x60090650U, \
    0x65014886U,     0x03020403U,     0xf1ee4004U,     0x799a0dcdU, \
    0x1c6d191fU,     0x18041929U,     0xc8d8ba6cU,     0x461108eeU, \
    0xd5c3acc2U,     0xa32ddbf1U,     0x0cdebaaaU,     0x5cf32f18U, \
    0x88d85746U,     0xd0374215U,     0xabf6972aU,     0x5922342dU, \
    0x0594b47aU,     0x2658f731U,     0x01025708U,     0x0618301dU, \
    0x01062b09U,     0x26820104U,     0x0b042301U,     0x04040930U, \
    0x00000000U,     0x30020102U,     0x5503061dU,     0x16040e1dU, \
    0xdf041404U,     0x1484b3bbU,     0xcb0c2577U,     0x7da4332eU, \
    0x1259854dU,     0x0d30e5fbU,     0x862a0906U,     0x0df78648U, \
    0x050d0101U,     0x02820300U,     0x37720001U,     0x14b7e357U, \
    0xe9d888a6U,     0x10a7ee3fU,     0x195e377eU,     0x611d45dbU, \
    0x9ef544fbU,     0x57831c68U,     0xb11b6720U,     0xe9efa870U, \
    0x8ffac448U,     0x253d6fcdU,     0x7b260926U,     0xed09d432U, \
    0x15de5822U,     0xf14e7f68U,     0x481242efU,     0xb73222a6U, \
    0x7cf37504U,     0x221df0f8U,     0xcc85cbd6U,     0xa030b2e9U, \
    0x0f3bc6dcU,     0x197958f7U,     0xafdfd163U,     0xc3517b96U, \
    0x16fe46a1U,     0x8a41f4aaU,     0xcd4fcca5U,     0xee7b893bU, \
    0x42458eb3U,     0x0f642ebdU,     0xbc963c6cU,     0xcb898442U, \
    0x126b412cU,     0xd693e3f7U,     0x45c81388U,     0x7d08a740U, \
    0x5eae8b62U,     0x2c89d4b1U,     0x8ee0f7fdU,     0x12ea2f40U, \
    0x7c95f087U,     0x8a0c4d87U,     0x3609b07bU,     0x11138b09U, \
    0x7e18fd8eU,     0xdc1f8267U,     0xfdd9aaf2U,     0xdae9f138U, \
    0x3cf6a4e3U,     0xeab99e03U,     0x45dabf46U,     0xd4c655aaU, \
    0x49661d24U,     0x523884c1U,     0x42da858cU,     0xeb3cdddbU, \
    0xf031053cU,     0x55fb37f3U,     0x103a07aaU,     0x672ff958U, \
    0xe34cb18cU,     0xe947e271U,     0x1e28f544U,     0x6edeed20U, \
    0x1e87f6a6U,     0x973865dbU,     0x8f4ab5c9U,     0xbc2ab43dU, \
    0x6fa4cf1bU,     0xf8bd5473U,     0x38d9b766U,     0x648dc457U, \
    0xbc2906eeU,     0x2a7b9501U,     0xab3fe78dU,     0xa1128520U, \
    0x4d54ca87U,     0x38fbae53U,     0xac870517U,     0x7e14b5c2U, \
    0x3cea45eaU,     0x55a62044U,     0x242012e1U,     0x3bded269U, \
    0x612ae348U,     0x8e35a928U,     0x6cb8f72dU,     0xefb1650eU, \
    0x652678abU,     0xae13fc55U,     0x2e083357U,     0x800cec2bU, \
    0xb1a004b5U,     0x735d476fU,     0x12463184U,     0xfde803e6U, \
    0x3a1a77cbU,     0xe03e8bc9U,     0xdc5aa4f4U,     0xd6f97333U, \
    0x5a353606U,     0xa9db8ad2U,     0x4e6cdeddU,     0x0f3f40f9U, \
    0xb3ab8dfdU,     0x98dc0382U,     0x30d764b1U,     0xb15fa483U, \
    0x0b2ec195U,     0x55c9d3a4U,     0xe20223dcU,     0x50ebd23fU, \
    0x4a29d19aU,     0x49dbe0b9U,     0x3abe90d5U,     0x877cec12U, \
    0xd0e832beU,     0xfe865510U,     0x4a8eef00U,     0x1dd07921U, \
    0xb1fda0b3U,     0x44a43247U,     0x90910353U,     0x5db7f329U, \
    0xca085768U,     0x6a9ce3c5U,     0x0100d645U,     0x0007c1d3U, \
    0x0700045aU,     0x01000712U,     0xa5c30001U,     0x0c000005U, \
    0x00000802U,     0x00000000U\
} /* 1671 bytes */

#endif
