/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
*  \file sciclient_defaultBoardcfg_hexhs.h
*
*  \brief File containing the Binary in a C array.
*
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#ifndef SCICLIENT_DEFAULTBOARDCFG_HEXHS_H_
#define SCICLIENT_DEFAULTBOARDCFG_HEXHS_H_


#define SCICLIENT_BOARDCFG_SIZE_IN_BYTES (1671U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define SCICLIENT_BOARDCFG { \
    0x66068230U,     0x4e048230U,     0x010203a0U,     0x6a140202U, \
    0x4669f497U,     0x1bacbc13U,     0xa2d85a64U,     0x91ef308eU, \
    0x305bf195U,     0x2a09060dU,     0xf7864886U,     0x0d01010dU, \
    0x81300005U,     0x300b318bU,     0x55030609U,     0x02130604U, \
    0x0b315355U,     0x03060930U,     0x0c080455U,     0x31435302U, \
    0x060d300fU,     0x07045503U,     0x6144060cU,     0x73616c6cU, \
    0x1f302131U,     0x04550306U,     0x54180c0aU,     0x73617865U, \
    0x736e4920U,     0x6d757274U,     0x73746e65U,     0x49202c2eU, \
    0x312e636eU,     0x060a300cU,     0x0b045503U,     0x4250030cU, \
    0x300f3155U,     0x5503060dU,     0x060c0304U,     0x65626c41U, \
    0x1c317472U,     0x09061a30U,     0x8648862aU,     0x09010df7U, \
    0x410d1601U,     0x7265626cU,     0x69744074U,     0x6d6f632eU, \
    0x0d171e30U,     0x33303532U,     0x34303430U,     0x30333534U, \
    0x320d175aU,     0x30343035U,     0x34343033U,     0x5a303335U, \
    0x318b8130U,     0x0609300bU,     0x06045503U,     0x53550213U, \
    0x09300b31U,     0x04550306U,     0x53020c08U,     0x300f3143U, \
    0x5503060dU,     0x060c0704U,     0x6c6c6144U,     0x21317361U, \
    0x03061f30U,     0x0c0a0455U,     0x78655418U,     0x49207361U, \
    0x7274736eU,     0x6e656d75U,     0x2c2e7374U,     0x636e4920U, \
    0x300c312eU,     0x5503060aU,     0x030c0b04U,     0x31554250U, \
    0x060d300fU,     0x03045503U,     0x6c41060cU,     0x74726562U, \
    0x1a301c31U,     0x862a0906U,     0x0df78648U,     0x16010901U, \
    0x626c410dU,     0x40747265U,     0x632e6974U,     0x82306d6fU, \
    0x0d302202U,     0x862a0906U,     0x0df78648U,     0x05010101U, \
    0x02820300U,     0x8230000fU,     0x82020a02U,     0xbf000102U, \
    0xd849ae14U,     0x6bd3727fU,     0x48ebcd23U,     0x22dc650eU, \
    0x4f0ef24dU,     0xb5edf682U,     0x7cdbddf2U,     0x596efa91U, \
    0xb6f7d5ffU,     0x8a1d04deU,     0xd995d2ccU,     0xc1c4e0d1U, \
    0xffbf50f8U,     0x22910c48U,     0x7b4c9a50U,     0x0a96f38bU, \
    0xa4b32628U,     0x55a9e0d9U,     0x3efb1a41U,     0xbf6c275bU, \
    0xaf71c0caU,     0xee22722fU,     0x62250146U,     0x04c73eadU, \
    0xb618b1f6U,     0x6e12c02cU,     0x3e9be20fU,     0xa8a0a6e5U, \
    0x41034506U,     0x1f164e17U,     0x84d674a9U,     0xa779d64eU, \
    0xa911b810U,     0x251f920eU,     0xf2b17fddU,     0x68f2b9d1U, \
    0x4b5933d8U,     0xcc777d82U,     0x23fa9cd1U,     0x8858fbb4U, \
    0xd5eacdf2U,     0x752cf216U,     0xc362fa2dU,     0xe06e09c1U, \
    0xb5e07006U,     0x62990907U,     0xe7e4d6d9U,     0x82c86d6cU, \
    0xf7935007U,     0xd1edd8e2U,     0x9ed0e35fU,     0xd95493cfU, \
    0xce5ddd5fU,     0xabf16037U,     0x7b048a14U,     0x7fbaa765U, \
    0x7c4545dfU,     0xae5ba14bU,     0x3d94c64eU,     0xd2874e8cU, \
    0xf3a43c94U,     0xf2fcda9fU,     0x0de77c36U,     0x37425aadU, \
    0xd0812af1U,     0x67a7a16eU,     0xed871e03U,     0x4a73bb00U, \
    0xa2312868U,     0x04a39a82U,     0xff87e8c1U,     0xc1aa7e45U, \
    0x053bd49fU,     0x21fd83c7U,     0x7fbdfe71U,     0x1916c938U, \
    0x03e60e52U,     0x1e1d8d33U,     0xcd1c36c9U,     0x29829d4eU, \
    0x2a9bcd88U,     0x7b5f6cbeU,     0x793ab2b2U,     0xf57d6a00U, \
    0x1e9d1aadU,     0xcf2a58cdU,     0x804ef45eU,     0xdd4f3babU, \
    0x34ded4f8U,     0xd920c4a2U,     0x852d1959U,     0x681f5e02U, \
    0xb98d4cb1U,     0x2de90611U,     0x8c58b576U,     0x6e37a850U, \
    0x836f7866U,     0x344d4630U,     0x4a18b49fU,     0x7bfabbb9U, \
    0x32d6aec5U,     0x6c848410U,     0x33809a3fU,     0xbc4dfc35U, \
    0x54606ed5U,     0x6d7ecf50U,     0xfa049780U,     0xfd200b8fU, \
    0xa12b98bdU,     0xfd59bd37U,     0xa145ec4aU,     0xc9178b09U, \
    0xb7331472U,     0x5d125e05U,     0xce1d5ae2U,     0xe1f65421U, \
    0xaa55d5eaU,     0x094deb27U,     0x824019dfU,     0x1789662eU, \
    0xb36ed965U,     0x8d4e38d6U,     0x74d61661U,     0x5f16ded4U, \
    0x42d51951U,     0xc8d283b8U,     0x69a94bdeU,     0x028db697U, \
    0x01000103U,     0x30bf81a3U,     0x0c30bc81U,     0x1d550306U, \
    0x30050413U,     0xff010103U,     0x09061230U,     0x0401062bU, \
    0x01268201U,     0x30050403U,     0x01010203U,     0x09065f30U, \
    0x0401062bU,     0x01268201U,     0x30520422U,     0x60090650U, \
    0x65014886U,     0x03020403U,     0xf1ee4004U,     0x799a0dcdU, \
    0x1c6d191fU,     0x18041929U,     0xc8d8ba6cU,     0x461108eeU, \
    0xd5c3acc2U,     0xa32ddbf1U,     0x0cdebaaaU,     0x5cf32f18U, \
    0x88d85746U,     0xd0374215U,     0xabf6972aU,     0x5922342dU, \
    0x0594b47aU,     0x2658f731U,     0x01025708U,     0x0618301dU, \
    0x01062b09U,     0x26820104U,     0x0b042301U,     0x04040930U, \
    0x00000000U,     0x30020102U,     0x5503061dU,     0x16040e1dU, \
    0xdf041404U,     0x1484b3bbU,     0xcb0c2577U,     0x7da4332eU, \
    0x1259854dU,     0x0d30e5fbU,     0x862a0906U,     0x0df78648U, \
    0x050d0101U,     0x02820300U,     0x5f8b0001U,     0x625d8befU, \
    0xbad1d85cU,     0xaf7a1201U,     0xf99c383bU,     0xa1c3f41bU, \
    0x608f25cfU,     0x5594b62fU,     0x27380948U,     0x2f256617U, \
    0xb0126f88U,     0x4d97b26cU,     0x37379ff5U,     0x6b826417U, \
    0x70557410U,     0xe68df58eU,     0x8844ec7cU,     0x281a97f2U, \
    0x0690d658U,     0xbcd19167U,     0x61632b85U,     0x6ac7086bU, \
    0x1c9a8082U,     0x5b5297b4U,     0xfd98a2b2U,     0x2b3c6c78U, \
    0xabb6bd7dU,     0xfffe5fe0U,     0xfc07fcd2U,     0x6529e653U, \
    0x8a855ca9U,     0x91d03006U,     0x7158cf85U,     0xd7ed2427U, \
    0x16fde11dU,     0xf6b32943U,     0xb5ca6e78U,     0x2e5b9776U, \
    0x03541752U,     0xd00d71c3U,     0xd60abaf8U,     0x31c50532U, \
    0x17467edbU,     0xaf687b33U,     0xca3b088aU,     0xc65c63d5U, \
    0x8b7eb8e8U,     0xa92a2175U,     0x9cc0665dU,     0x55c8623cU, \
    0xd759c3ffU,     0xca97395bU,     0x2963eca8U,     0x57dd2c11U, \
    0xd5f37ca4U,     0x4c8c0f5dU,     0x41568e39U,     0x698ee04bU, \
    0x2816fb41U,     0xb07cf9bbU,     0x25b97179U,     0x94721593U, \
    0x16f36b16U,     0xf7cba1afU,     0x5d5bc4cfU,     0x78e1f7d4U, \
    0x9eb83aa3U,     0xc4dc2596U,     0x8b664ae5U,     0xd3407ca0U, \
    0x5a2403ffU,     0x2da74391U,     0xa8117d4cU,     0x2af32361U, \
    0x6d7e3b6cU,     0x4c01bfa2U,     0xa985800fU,     0x2efc536bU, \
    0xb3164199U,     0x179256e8U,     0x8f23dc63U,     0x65d5e4e9U, \
    0xc51c7bfbU,     0xf1ad6dfbU,     0x5aa7fb02U,     0x77879d4aU, \
    0x39d6f6e1U,     0x9a244f1aU,     0xdaa0a03aU,     0x149c73e6U, \
    0xb4e69c74U,     0x121d0040U,     0xa3b407c5U,     0x8fd7c9a4U, \
    0xb53b612bU,     0x4175eb14U,     0x46c2a284U,     0x7a9612c5U, \
    0xfbf57275U,     0x2588dc00U,     0x2f432edbU,     0xa2ba2708U, \
    0xc2d385d4U,     0xb19641e5U,     0xdfc303dfU,     0x2ead59dfU, \
    0xf661d25bU,     0xa038dd1aU,     0x28463288U,     0x09b898adU, \
    0xa240a1fdU,     0x343b03b4U,     0xf646266dU,     0xb40da0bbU, \
    0xd1c11a7cU,     0xee68086dU,     0xce566c5aU,     0xc3e65e89U, \
    0x2b4e1b15U,     0x9df10f0bU,     0xd557783aU,     0xb618708cU, \
    0xfb553a4fU,     0x50075471U,     0x39453171U,     0x92a4aa0eU, \
    0xdf444b81U,     0x38adf12cU,     0x0100fd06U,     0x0007c1d3U, \
    0x0700045aU,     0x01000712U,     0xa5c30001U,     0x0c000005U, \
    0x00000802U,     0x00000000U\
} /* 1671 bytes */

#endif
