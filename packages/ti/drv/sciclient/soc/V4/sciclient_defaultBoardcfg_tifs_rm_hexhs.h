/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
*  \file sciclient_defaultBoardcfg_tifs_rm_hexhs.h
*
*  \brief File containing the Binary in a C array.
*
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#ifndef SCICLIENT_DEFAULTBOARDCFG_TIFS_RM_HEXHS_H_
#define SCICLIENT_DEFAULTBOARDCFG_TIFS_RM_HEXHS_H_


#define SCICLIENT_BOARDCFG_TIFS_RM_SIZE_IN_BYTES (4545U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define SCICLIENT_BOARDCFG_TIFS_RM { \
    0x67068230U,     0x4f048230U,     0x010203a0U,     0x7d140202U, \
    0x4811a5c6U,     0xe04fbd23U,     0x5832515eU,     0xc9ba6ae0U, \
    0x30a0f092U,     0x2a09060dU,     0xf7864886U,     0x0d01010dU, \
    0x81300005U,     0x300b318bU,     0x55030609U,     0x02130604U, \
    0x0b315355U,     0x03060930U,     0x0c080455U,     0x31435302U, \
    0x060d300fU,     0x07045503U,     0x6144060cU,     0x73616c6cU, \
    0x1f302131U,     0x04550306U,     0x54180c0aU,     0x73617865U, \
    0x736e4920U,     0x6d757274U,     0x73746e65U,     0x49202c2eU, \
    0x312e636eU,     0x060a300cU,     0x0b045503U,     0x4250030cU, \
    0x300f3155U,     0x5503060dU,     0x060c0304U,     0x65626c41U, \
    0x1c317472U,     0x09061a30U,     0x8648862aU,     0x09010df7U, \
    0x410d1601U,     0x7265626cU,     0x69744074U,     0x6d6f632eU, \
    0x0d171e30U,     0x33303532U,     0x34303430U,     0x30333534U, \
    0x320d175aU,     0x30343035U,     0x34343033U,     0x5a303335U, \
    0x318b8130U,     0x0609300bU,     0x06045503U,     0x53550213U, \
    0x09300b31U,     0x04550306U,     0x53020c08U,     0x300f3143U, \
    0x5503060dU,     0x060c0704U,     0x6c6c6144U,     0x21317361U, \
    0x03061f30U,     0x0c0a0455U,     0x78655418U,     0x49207361U, \
    0x7274736eU,     0x6e656d75U,     0x2c2e7374U,     0x636e4920U, \
    0x300c312eU,     0x5503060aU,     0x030c0b04U,     0x31554250U, \
    0x060d300fU,     0x03045503U,     0x6c41060cU,     0x74726562U, \
    0x1a301c31U,     0x862a0906U,     0x0df78648U,     0x16010901U, \
    0x626c410dU,     0x40747265U,     0x632e6974U,     0x82306d6fU, \
    0x0d302202U,     0x862a0906U,     0x0df78648U,     0x05010101U, \
    0x02820300U,     0x8230000fU,     0x82020a02U,     0xbf000102U, \
    0xd849ae14U,     0x6bd3727fU,     0x48ebcd23U,     0x22dc650eU, \
    0x4f0ef24dU,     0xb5edf682U,     0x7cdbddf2U,     0x596efa91U, \
    0xb6f7d5ffU,     0x8a1d04deU,     0xd995d2ccU,     0xc1c4e0d1U, \
    0xffbf50f8U,     0x22910c48U,     0x7b4c9a50U,     0x0a96f38bU, \
    0xa4b32628U,     0x55a9e0d9U,     0x3efb1a41U,     0xbf6c275bU, \
    0xaf71c0caU,     0xee22722fU,     0x62250146U,     0x04c73eadU, \
    0xb618b1f6U,     0x6e12c02cU,     0x3e9be20fU,     0xa8a0a6e5U, \
    0x41034506U,     0x1f164e17U,     0x84d674a9U,     0xa779d64eU, \
    0xa911b810U,     0x251f920eU,     0xf2b17fddU,     0x68f2b9d1U, \
    0x4b5933d8U,     0xcc777d82U,     0x23fa9cd1U,     0x8858fbb4U, \
    0xd5eacdf2U,     0x752cf216U,     0xc362fa2dU,     0xe06e09c1U, \
    0xb5e07006U,     0x62990907U,     0xe7e4d6d9U,     0x82c86d6cU, \
    0xf7935007U,     0xd1edd8e2U,     0x9ed0e35fU,     0xd95493cfU, \
    0xce5ddd5fU,     0xabf16037U,     0x7b048a14U,     0x7fbaa765U, \
    0x7c4545dfU,     0xae5ba14bU,     0x3d94c64eU,     0xd2874e8cU, \
    0xf3a43c94U,     0xf2fcda9fU,     0x0de77c36U,     0x37425aadU, \
    0xd0812af1U,     0x67a7a16eU,     0xed871e03U,     0x4a73bb00U, \
    0xa2312868U,     0x04a39a82U,     0xff87e8c1U,     0xc1aa7e45U, \
    0x053bd49fU,     0x21fd83c7U,     0x7fbdfe71U,     0x1916c938U, \
    0x03e60e52U,     0x1e1d8d33U,     0xcd1c36c9U,     0x29829d4eU, \
    0x2a9bcd88U,     0x7b5f6cbeU,     0x793ab2b2U,     0xf57d6a00U, \
    0x1e9d1aadU,     0xcf2a58cdU,     0x804ef45eU,     0xdd4f3babU, \
    0x34ded4f8U,     0xd920c4a2U,     0x852d1959U,     0x681f5e02U, \
    0xb98d4cb1U,     0x2de90611U,     0x8c58b576U,     0x6e37a850U, \
    0x836f7866U,     0x344d4630U,     0x4a18b49fU,     0x7bfabbb9U, \
    0x32d6aec5U,     0x6c848410U,     0x33809a3fU,     0xbc4dfc35U, \
    0x54606ed5U,     0x6d7ecf50U,     0xfa049780U,     0xfd200b8fU, \
    0xa12b98bdU,     0xfd59bd37U,     0xa145ec4aU,     0xc9178b09U, \
    0xb7331472U,     0x5d125e05U,     0xce1d5ae2U,     0xe1f65421U, \
    0xaa55d5eaU,     0x094deb27U,     0x824019dfU,     0x1789662eU, \
    0xb36ed965U,     0x8d4e38d6U,     0x74d61661U,     0x5f16ded4U, \
    0x42d51951U,     0xc8d283b8U,     0x69a94bdeU,     0x028db697U, \
    0x01000103U,     0x30c081a3U,     0x0c30bd81U,     0x1d550306U, \
    0x30050413U,     0xff010103U,     0x09061230U,     0x0401062bU, \
    0x01268201U,     0x30050403U,     0x01010203U,     0x09066030U, \
    0x0401062bU,     0x01268201U,     0x30530422U,     0x60090651U, \
    0x65014886U,     0x03020403U,     0x5e8d4004U,     0xf13bf8b5U, \
    0xc67d0cc5U,     0x45e29f9bU,     0x192abb1eU,     0x4572fe40U, \
    0x4b72acbbU,     0xf958e51dU,     0xe8772876U,     0x73b3dcabU, \
    0xa25a3daeU,     0x4806c60dU,     0xf1b528f9U,     0xe68f3b8aU, \
    0xf7b51de3U,     0x6a382b39U,     0x0202c0a4U,     0x1830560bU, \
    0x062b0906U,     0x82010401U,     0x04230126U,     0x0409300bU, \
    0x00000004U,     0x02010200U,     0x03061d30U,     0x040e1d55U, \
    0x04140416U,     0x84b3bbdfU,     0x0c257714U,     0xa4332ecbU, \
    0x59854d7dU,     0x30e5fb12U,     0x2a09060dU,     0xf7864886U, \
    0x0d01010dU,     0x82030005U,     0x9d000102U,     0xea8a0392U, \
    0x1d5bbebfU,     0x5d9a4d57U,     0x2e510f1bU,     0x4f09a6b4U, \
    0xeeb1d0a2U,     0x1f65e1e1U,     0x0c6a8c39U,     0x5f813cccU, \
    0x61ab65aaU,     0x99b4557aU,     0x9f57b846U,     0xf7d73b7dU, \
    0x7368cf06U,     0x6d3ba3caU,     0x54635696U,     0xe1364cc7U, \
    0x347d8ed6U,     0xaecbfc12U,     0x0d5bc853U,     0x5f5c43c1U, \
    0x1eeeb677U,     0xf3edf949U,     0x6922aeaaU,     0xa87ae33bU, \
    0x191f9db8U,     0xd500492cU,     0x2a38b819U,     0xdb49f30dU, \
    0x5e1a64c1U,     0x5ad9a507U,     0x2563b153U,     0x6ccfc92dU, \
    0x7eb91267U,     0x9dc097c2U,     0x3e6a9f1eU,     0xce37435bU, \
    0x1b26725cU,     0x624f5eeeU,     0x5e2f2760U,     0x4611898cU, \
    0x2ae17f8fU,     0x75917cffU,     0xa3ce48f0U,     0x59b34f53U, \
    0x448e8654U,     0x37b9b5cbU,     0xa4a0922bU,     0x1776014eU, \
    0xa05f4c29U,     0x2450c25bU,     0xe692f5acU,     0xadb22d07U, \
    0x8e7a8620U,     0xb72c41ddU,     0xc06e8435U,     0xabb2a1d7U, \
    0x86ceddbaU,     0x2b90d50eU,     0xaf5ec01fU,     0xc4c1ae87U, \
    0xac2cfcb8U,     0x75858a5cU,     0xeb5dfbe6U,     0xe0b1c459U, \
    0x10623286U,     0x2a006adaU,     0xe1f00ca2U,     0x4f00b817U, \
    0x5aa7c4e8U,     0xb7f90af7U,     0x78329b2aU,     0x0e2e206eU, \
    0x95cbc26bU,     0xc2a1ed2bU,     0xf8d91293U,     0x19cb1775U, \
    0x1d602ae8U,     0x10bb735bU,     0x782058c8U,     0x7c2345bbU, \
    0xd3fa2c30U,     0x395fa047U,     0x42b60521U,     0x8de18d89U, \
    0xec2fe9cdU,     0x04724820U,     0xb060ea1eU,     0x2f5695caU, \
    0xa1d24c48U,     0x7109a1e5U,     0x863ce21dU,     0x806861f9U, \
    0x69d87d4bU,     0x5779a1faU,     0xf0cd42e9U,     0x7a28fc5aU, \
    0x60b065aeU,     0x74993023U,     0xc6731735U,     0x2392c0f0U, \
    0x3afe8937U,     0x57ab2262U,     0xee685fe0U,     0xa270aba7U, \
    0xc0f1e726U,     0x259f587eU,     0xa82ce85eU,     0x6e230890U, \
    0x1bc58fa6U,     0xbc370146U,     0x44f2d212U,     0x8be4d131U, \
    0xececc77dU,     0x41b04f41U,     0x2da1065cU,     0x6903c53aU, \
    0x5b4e3992U,     0x9354c316U,     0x340eda81U,     0xf7a7f3d5U, \
    0x815f59caU,     0x9150546eU,     0x257384a1U,     0x497d0152U, \
    0xec5a5d64U,     0x9f05a96fU,     0x000b1e7bU,     0x644c4101U, \
    0xaa2a0301U,     0xaaaaaaaaU,     0xaaaaaaaaU,     0xaaaa2a05U, \
    0xaaaaaaaaU,     0x0caaaaaaU,     0xaaaaaa2aU,     0xaaaaaaaaU, \
    0x2a0daaaaU,     0xaaaaaaaaU,     0xaaaaaaaaU,     0xaa2a15aaU, \
    0xaaaaaaaaU,     0xaaaaaaaaU,     0xaaaa2a17U,     0xaaaaaaaaU, \
    0x23aaaaaaU,     0xaaaaaa2aU,     0xaaaaaaaaU,     0x2a25aaaaU, \
    0xaaaaaaaaU,     0xaaaaaaaaU,     0xaa2a28aaU,     0xaaaaaaaaU, \
    0xaaaaaaaaU,     0xaaaa2a2aU,     0xaaaaaaaaU,     0x00aaaaaaU, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x087b2500U,     0x0009e800U,     0x10001000U,     0x0c384e00U, \
    0x10002000U,     0x23384e00U,     0x08000000U,     0x0c384f00U, \
    0x08000800U,     0x23384f00U,     0x10000000U,     0x0c386100U, \
    0x10001000U,     0x23386100U,     0x08000000U,     0x0c386200U, \
    0x08000800U,     0x23386200U,     0x40000000U,     0x803f8a00U, \
    0x40000000U,     0x803fca00U,     0x04000000U,     0x0c400000U, \
    0x04000400U,     0x0d400000U,     0x04000800U,     0x15400000U, \
    0x04000c00U,     0x17400000U,     0x0c001000U,     0x23400000U, \
    0x04001c00U,     0x25400000U,     0x04002000U,     0x28400000U, \
    0x04002400U,     0x2a400000U,     0x04002800U,     0x03400000U, \
    0x04002c00U,     0x05400000U,     0x10003000U,     0x80400000U, \
    0x01000000U,     0x8040c000U,     0x2001a700U,     0x1540c100U, \
    0x2001c700U,     0x1740c100U,     0xb601e700U,     0x2340c100U, \
    0x28029d00U,     0x2540c100U,     0x0a02c500U,     0x2840c100U, \
    0x0a02cf00U,     0x2a40c100U,     0x0602d900U,     0x0340c100U, \
    0x0602df00U,     0x0540c100U,     0x80036e00U,     0x0c40c100U, \
    0x0a03ee00U,     0x0d40c100U,     0x06015900U,     0x0c40c200U, \
    0x00015f00U,     0x0d40c200U,     0x02015f00U,     0x1540c200U, \
    0x02016100U,     0x1740c200U,     0x06016300U,     0x2340c200U, \
    0x01016900U,     0x2540c200U,     0x01016a00U,     0x2840c200U, \
    0x01016b00U,     0x2a40c200U,     0x02016c00U,     0x0340c200U, \
    0x02016e00U,     0x0540c200U,     0x16017000U,     0x0c40c200U, \
    0x06018600U,     0x0d40c200U,     0x04018c00U,     0x1540c200U, \
    0x04019000U,     0x1740c200U,     0x0c019400U,     0x2340c200U, \
    0x0101a000U,     0x2540c200U,     0x0201a100U,     0x2840c200U, \
    0x0201a300U,     0x2a40c200U,     0x0201a500U,     0x8040c200U, \
    0x06000400U,     0x0c40c300U,     0x00000a00U,     0x0d40c300U, \
    0x02000a00U,     0x1540c300U,     0x02000c00U,     0x1740c300U, \
    0x06000e00U,     0x2340c300U,     0x01001400U,     0x2540c300U, \
    0x01001500U,     0x2840c300U,     0x01001600U,     0x2a40c300U, \
    0x02001700U,     0x0340c300U,     0x02001900U,     0x0540c300U, \
    0x16001b00U,     0x0c40c300U,     0x06003100U,     0x0d40c300U, \
    0x04003700U,     0x1540c300U,     0x04003b00U,     0x1740c300U, \
    0x0c003f00U,     0x2340c300U,     0x04004b00U,     0x2540c300U, \
    0x02004f00U,     0x2840c300U,     0x02005100U,     0x2a40c300U, \
    0x02005300U,     0x8040c300U,     0x10005500U,     0x1540c400U, \
    0x0c006500U,     0x1740c400U,     0x02007100U,     0x2340c400U, \
    0x02007300U,     0x2540c400U,     0x60007500U,     0x2340c400U, \
    0x20013500U,     0x2540c400U,     0x01015700U,     0x0c40c500U, \
    0x01015800U,     0x2340c500U,     0x01015500U,     0x0c40c600U, \
    0x01015600U,     0x2340c600U,     0x01000200U,     0x0c40c700U, \
    0x01000300U,     0x2340c700U,     0x01000000U,     0x0c40c800U, \
    0x01000100U,     0x2340c800U,     0x05000200U,     0x0c40ca00U, \
    0x01000700U,     0x0d40ca00U,     0x03000000U,     0x0c40cb00U, \
    0x02000300U,     0x0d40cb00U,     0x03000500U,     0x1540cb00U, \
    0x03000800U,     0x1740cb00U,     0x06000b00U,     0x2340cb00U, \
    0x03001100U,     0x2540cb00U,     0x03001400U,     0x2840cb00U, \
    0x03001700U,     0x2a40cb00U,     0x01001a00U,     0x0340cb00U, \
    0x01001b00U,     0x0540cb00U,     0x04001c00U,     0x8040cb00U, \
    0x10005200U,     0x0c41c000U,     0x10006200U,     0x0d41c000U, \
    0x6e007200U,     0x8041c000U,     0x01000000U,     0x8041c100U, \
    0x06000400U,     0x0c41ca00U,     0x00000a00U,     0x0d41ca00U, \
    0x02000a00U,     0x1541ca00U,     0x02000c00U,     0x1741ca00U, \
    0x06000e00U,     0x2341ca00U,     0x01001400U,     0x2541ca00U, \
    0x01001500U,     0x2841ca00U,     0x01001600U,     0x2a41ca00U, \
    0x02001700U,     0x0341ca00U,     0x02001900U,     0x0541ca00U, \
    0x16001b00U,     0x0c41ca00U,     0x06003100U,     0x0d41ca00U, \
    0x04003700U,     0x1541ca00U,     0x04003b00U,     0x1741ca00U, \
    0x0c003f00U,     0x2341ca00U,     0x01004b00U,     0x2541ca00U, \
    0x02004c00U,     0x2841ca00U,     0x02004e00U,     0x2a41ca00U, \
    0x02005000U,     0x8041ca00U,     0x01000200U,     0x0c41cb00U, \
    0x01000300U,     0x2341cb00U,     0x01000000U,     0x0c41cc00U, \
    0x01000100U,     0x2341cc00U,     0x06000400U,     0x0c41cd00U, \
    0x00000a00U,     0x0d41cd00U,     0x02000a00U,     0x1541cd00U, \
    0x02000c00U,     0x1741cd00U,     0x06000e00U,     0x2341cd00U, \
    0x01001400U,     0x2541cd00U,     0x01001500U,     0x2841cd00U, \
    0x01001600U,     0x2a41cd00U,     0x02001700U,     0x0341cd00U, \
    0x02001900U,     0x0541cd00U,     0x16001b00U,     0x0c41cd00U, \
    0x06003100U,     0x0d41cd00U,     0x04003700U,     0x1541cd00U, \
    0x04003b00U,     0x1741cd00U,     0x0c003f00U,     0x2341cd00U, \
    0x04004b00U,     0x2541cd00U,     0x02004f00U,     0x2841cd00U, \
    0x02005100U,     0x2a41cd00U,     0x02005300U,     0x8041cd00U, \
    0x10005500U,     0x1541ce00U,     0x0c006500U,     0x1741ce00U, \
    0x02007100U,     0x2341ce00U,     0x02007300U,     0x2541ce00U, \
    0x60007500U,     0x2341ce00U,     0x20013500U,     0x2541ce00U, \
    0x01000200U,     0x0c41cf00U,     0x01000300U,     0x2341cf00U, \
    0x01000000U,     0x0c41d000U,     0x01000100U,     0x2341d000U, \
    0x56002200U,     0x0c424a00U,     0x20007800U,     0x0d424a00U, \
    0x0c009800U,     0x15424a00U,     0x0c00a400U,     0x17424a00U, \
    0x1c00b000U,     0x23424a00U,     0x0800cc00U,     0x25424a00U, \
    0x0c00d400U,     0x28424a00U,     0x0c00e000U,     0x2a424a00U, \
    0x1400ec00U,     0x80424a00U,     0x10060000U,     0x80424f00U, \
    0x10080000U,     0x80425000U,     0x100a0000U,     0x80425100U, \
    0x200c0000U,     0x80425200U,     0x200e0000U,     0x80425300U, \
    0x20100000U,     0x80425400U,     0x04000100U,     0x0c43c000U, \
    0x04000500U,     0x0d43c000U,     0x04000900U,     0x1543c000U, \
    0x04000d00U,     0x1743c000U,     0x10001100U,     0x2343c000U, \
    0x04002100U,     0x2543c000U,     0x04002500U,     0x2843c000U, \
    0x04002900U,     0x2a43c000U,     0x04002d00U,     0x0343c000U, \
    0x04003100U,     0x0543c000U,     0x0b003500U,     0x8043c000U, \
    0x01000000U,     0x80440000U,     0x14006000U,     0x0c440100U, \
    0x08007400U,     0x0d440100U,     0x08007c00U,     0x15440100U, \
    0x08008400U,     0x17440100U,     0x10008c00U,     0x23440100U, \
    0x08009c00U,     0x25440100U,     0x0800a400U,     0x28440100U, \
    0x0800ac00U,     0x2a440100U,     0x2000b400U,     0x03440100U, \
    0x0c00d400U,     0x05440100U,     0x1c00e000U,     0x80440100U, \
    0x04003200U,     0x0c440200U,     0x00003600U,     0x0d440200U, \
    0x01003600U,     0x15440200U,     0x01003700U,     0x17440200U, \
    0x01003800U,     0x23440200U,     0x01003900U,     0x25440200U, \
    0x01003a00U,     0x28440200U,     0x01003b00U,     0x2a440200U, \
    0x02003c00U,     0x03440200U,     0x00003e00U,     0x05440200U, \
    0x09003e00U,     0x0c440200U,     0x06004700U,     0x0d440200U, \
    0x01004d00U,     0x15440200U,     0x01004e00U,     0x17440200U, \
    0x02004f00U,     0x23440200U,     0x01005100U,     0x25440200U, \
    0x01005200U,     0x28440200U,     0x01005300U,     0x2a440200U, \
    0x03005400U,     0x03440200U,     0x02005700U,     0x05440200U, \
    0x04005900U,     0x80440200U,     0x04000200U,     0x0c440300U, \
    0x00000600U,     0x0d440300U,     0x01000600U,     0x15440300U, \
    0x01000700U,     0x17440300U,     0x01000800U,     0x23440300U, \
    0x01000900U,     0x25440300U,     0x01000a00U,     0x28440300U, \
    0x01000b00U,     0x2a440300U,     0x02000c00U,     0x03440300U, \
    0x00000e00U,     0x05440300U,     0x09000e00U,     0x0c440300U, \
    0x06001700U,     0x0d440300U,     0x01001d00U,     0x15440300U, \
    0x01001e00U,     0x17440300U,     0x02001f00U,     0x23440300U, \
    0x01002100U,     0x25440300U,     0x01002200U,     0x28440300U, \
    0x01002300U,     0x2a440300U,     0x03002400U,     0x03440300U, \
    0x02002700U,     0x05440300U,     0x05002900U,     0x80440300U, \
    0x00003000U,     0x03440500U,     0x02003000U,     0x03440500U, \
    0x00000000U,     0x03440700U,     0x02000000U,     0x03440700U, \
    0x05000200U,     0x0c440a00U,     0x01000700U,     0x0d440a00U, \
    0x03000000U,     0x0c440b00U,     0x02000300U,     0x0d440b00U, \
    0x03000500U,     0x15440b00U,     0x03000800U,     0x17440b00U, \
    0x03000b00U,     0x23440b00U,     0x03000e00U,     0x25440b00U, \
    0x03001100U,     0x28440b00U,     0x03001400U,     0x2a440b00U, \
    0x03001700U,     0x03440b00U,     0x03001a00U,     0x05440b00U, \
    0x03001d00U,     0x80440b00U,     0x08003000U,     0x0c444000U, \
    0x04003800U,     0x0d444000U,     0x08003c00U,     0x23444000U, \
    0x04004400U,     0x25444000U,     0x04004800U,     0x28444000U, \
    0x04004c00U,     0x2a444000U,     0x08005000U,     0x03444000U, \
    0x04005800U,     0x05444000U,     0x04005c00U,     0x80444000U, \
    0x01000000U,     0x80444100U,     0x04000200U,     0x0c444a00U, \
    0x00000600U,     0x0d444a00U,     0x01000600U,     0x15444a00U, \
    0x01000700U,     0x17444a00U,     0x01000800U,     0x23444a00U, \
    0x01000900U,     0x25444a00U,     0x01000a00U,     0x28444a00U, \
    0x01000b00U,     0x2a444a00U,     0x02000c00U,     0x03444a00U, \
    0x00000e00U,     0x05444a00U,     0x09000e00U,     0x0c444a00U, \
    0x06001700U,     0x0d444a00U,     0x01001d00U,     0x15444a00U, \
    0x01001e00U,     0x17444a00U,     0x02001f00U,     0x23444a00U, \
    0x01002100U,     0x25444a00U,     0x01002200U,     0x28444a00U, \
    0x01002300U,     0x2a444a00U,     0x03002400U,     0x03444a00U, \
    0x02002700U,     0x05444a00U,     0x04002900U,     0x80444a00U, \
    0x00000000U,     0x03444b00U,     0x02000000U,     0x03444b00U, \
    0x04000200U,     0x0c444d00U,     0x00000600U,     0x0d444d00U, \
    0x01000600U,     0x15444d00U,     0x01000700U,     0x17444d00U, \
    0x01000800U,     0x23444d00U,     0x01000900U,     0x25444d00U, \
    0x01000a00U,     0x28444d00U,     0x01000b00U,     0x2a444d00U, \
    0x02000c00U,     0x03444d00U,     0x00000e00U,     0x05444d00U, \
    0x09000e00U,     0x0c444d00U,     0x06001700U,     0x0d444d00U, \
    0x01001d00U,     0x15444d00U,     0x01001e00U,     0x17444d00U, \
    0x02001f00U,     0x23444d00U,     0x01002100U,     0x25444d00U, \
    0x01002200U,     0x28444d00U,     0x01002300U,     0x2a444d00U, \
    0x03002400U,     0x03444d00U,     0x02002700U,     0x05444d00U, \
    0x05002900U,     0x80444d00U,     0x00000000U,     0x03444f00U, \
    0x02000000U,     0x03444f00U,     0x20001600U,     0x0c44ca00U, \
    0x10003600U,     0x0d44ca00U,     0x08004600U,     0x1544ca00U, \
    0x08004e00U,     0x1744ca00U,     0x18005600U,     0x2344ca00U, \
    0x08006e00U,     0x2544ca00U,     0x10007600U,     0x2844ca00U, \
    0x10008600U,     0x2a44ca00U,     0x40009600U,     0x0344ca00U, \
    0x0400d600U,     0x0544ca00U,     0x2600da00U,     0x8044ca00U, \
    0x00000000U\
} /* 4545 bytes */

#endif
