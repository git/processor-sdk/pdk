=========================================
TISCI PM Low Power Mode API Documentation
=========================================
.. note::

  This document is applicable to AM62x, AM62Ax and AM62Px devices.

Introduction
============

This document describes the TISCI API for entering different low power modes
supported by the SOC.

Supported low power modes
-------------------------

.. sysfwapimacro:: TISCI_MSG_VALUE_SLEEP_MODE_DEEP_SLEEP

.. sysfwapimacro:: TISCI_MSG_VALUE_SLEEP_MODE_MCU_ONLY

.. sysfwapimacro:: TISCI_MSG_VALUE_SLEEP_MODE_IO_ONLY_PLUS_DDR

.. sysfwapimacro:: TISCI_MSG_VALUE_SLEEP_MODE_PARTIAL_IO

.. sysfwapimacro:: TISCI_MSG_VALUE_SLEEP_MODE_STANDBY

.. sysfwapimacro:: TISCI_MSG_VALUE_SLEEP_MODE_SOC_OFF

+------------------+-----------------------+
| Low power mode   |  Supported by         | 
+==================+=======================+
| DEEP_SLEEP       | am62x, am62ax, am62px | 
+------------------+-----------------------+
| MCU_ONLY         | am62x, am62ax, am62px | 
+------------------+-----------------------+
| IO_ONLY_PLUS_DDR | am62ax, am62px        | 
+------------------+-----------------------+
| PARTIAL_IO       | am62x, am62ax, am62px | 
+------------------+-----------------------+
| STANDBY          | None                  | 
+------------------+-----------------------+
| SOC_OFF          | j7200, j784s4         | 
+------------------+-----------------------+

Intermediate mode values
------------------------

.. sysfwapimacro:: TISCI_MSG_VALUE_SLEEP_MODE_DM_MANAGED

.. sysfwapimacro:: TISCI_MSG_VALUE_SLEEP_MODE_NOT_SELECTED

.. sysfwapimacro:: TISCI_MSG_VALUE_SLEEP_MODE_INVALID

Device configuration and control APIs
-------------------------------------

+------------------+--------------------------------------------+
| TISCI Message ID |             Message Name                   |
+==================+============================================+
| 0x0300           | :ref:`pm_lpm_msg_prepare_sleep`            |
+------------------+--------------------------------------------+
| 0x0301           | :ref:`pm_lpm_msg_enter_sleep`              |
+------------------+--------------------------------------------+
| 0x0306           | :ref:`pm_lpm_msg_wake_reason`              |
+------------------+--------------------------------------------+
| 0x0307           | :ref:`pm_lpm_msg_set_io_isolation`         |
+------------------+--------------------------------------------+
| 0x0308           | :ref:`pm_lpm_msg_min_context_restore`      |
+------------------+--------------------------------------------+
| 0x0309           | :ref:`pm_lpm_msg_set_device_constraint`    |
+------------------+--------------------------------------------+
| 0x030A           | :ref:`pm_lpm_msg_set_latency_constraint`   |
+------------------+--------------------------------------------+
| 0x030B           | :ref:`pm_lpm_msg_get_device_constraint`    |
+------------------+--------------------------------------------+
| 0x030C           | :ref:`pm_lpm_msg_get_latency_constraint`   |
+------------------+--------------------------------------------+
| 0x030D           | :ref:`pm_lpm_msg_get_next_sys_mode`        |
+------------------+--------------------------------------------+
| 0x030E           | :ref:`pm_lpm_msg_get_next_host_state`      |
+------------------+--------------------------------------------+
| 0x0311           | :ref:`pm_lpm_msg_lpm_abort`                |
+------------------+--------------------------------------------+

Macros Used in this Document
----------------------------

.. sysfwapimacro:: TISCI_MSG_VALUE_IO_ENABLE
.. sysfwapimacro:: TISCI_MSG_VALUE_IO_DISABLE
.. sysfwapimacro:: TISCI_MSG_VALUE_HOST_STATE_ON
.. sysfwapimacro:: TISCI_MSG_VALUE_HOST_STATE_OFF
.. sysfwapimacro:: TISCI_MSG_VALUE_HOST_STATE_INVALID
.. sysfwapimacro:: TISCI_MSG_VALUE_STATE_SET
.. sysfwapimacro:: TISCI_MSG_VALUE_STATE_CLEAR
.. sysfwapimacro:: TISCI_MSG_VALUE_LPM_WAKE_PIN_INVALID

.. _pm_lpm_msg_prepare_sleep:

TISCI_MSG_PREPARE_SLEEP
=======================

Objective
----------

Prepare the SOC for entering into a low power mode.

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | No     |
+------------------------+--------+



TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_PREPARE_SLEEP

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_prepare_sleep_req

.. sysfwapistruct:: tisci_msg_prepare_sleep_resp

.. _pm_lpm_msg_enter_sleep:

TISCI_MSG_ENTER_SLEEP
=====================

Objective
----------

Enter into a low power mode.

.. note::
  Before calling the TISCI_MSG_ENTER_SLEEP, TISCI_MSG_PREPARE_SLEEP should be
  called to prepare for the low power mode.

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | Yes    |
+------------------------+--------+

TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_ENTER_SLEEP

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_enter_sleep_req

.. sysfwapistruct:: tisci_msg_enter_sleep_resp


.. _pm_lpm_msg_wake_reason:

TISCI_MSG_LPM_WAKE_REASON
=========================

Objective
----------

Get the last entered low power mode, wake up source and pin (if 
applicable) that woke the soc.

Wake up sources
---------------

+------------------+------------+
| Wake Up Source   | Source ID  |
+==================+============+
| WKUP_I2C0        | 0x00       |
+------------------+------------+
| WKUP_UART0       | 0x10       |
+------------------+------------+
| MCU_GPIO0        | 0x20       |
+------------------+------------+
| WKUP_ICEMELTER0  | 0x30       |
+------------------+------------+
| WKUP_TIMER0      | 0x40       |
+------------------+------------+
| WKUP_TIMER1      | 0x41       |
+------------------+------------+
| WKUP_RTC0        | 0x50       |
+------------------+------------+
| RESET            | 0x60       |
+------------------+------------+
| USB0             | 0x70       |
+------------------+------------+
| USB1             | 0x71       |
+------------------+------------+
| MAIN_IO          | 0x80       |
+------------------+------------+
| MCU_IO           | 0x81       |
+------------------+------------+
| CAN_IO           | 0x82       |
+------------------+------------+
| MCU_IPC          | 0x90       |
+------------------+------------+
| INVALID          | 0xFF       |
+------------------+------------+

Wake up pin
-----------

Refer to the Pad Configuration PADCONFIG Registers in Registers chapter in 
the TRM to get information about pad number.

.. note::
  In case of wakeup event on multiple pins simultaneously/concurrently, the 
  smaller padcfg number will be returned.

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | No     |
+------------------------+--------+


TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_LPM_WAKE_REASON

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_lpm_wake_reason_req

.. sysfwapistruct:: tisci_msg_lpm_wake_reason_resp

.. _pm_lpm_msg_set_io_isolation:

TISCI_MSG_SET_IO_ISOLATION
==========================

Objective
---------

Enable and disable IO isolation

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | No     |
+------------------------+--------+

TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_SET_IO_ISOLATION

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_set_io_isolation_req

.. sysfwapistruct:: tisci_msg_set_io_isolation_resp

.. _pm_lpm_msg_min_context_restore:

TISCI_MSG_MIN_CONTEXT_RESTORE
=============================

Objective
----------

Restore the minimal context saved during lpm entry.

.. note::
  This message is registered only if IO Only Plus DDR low power mode 
  exit is detected from MMR read.

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | Yes    |
+------------------------+--------+

TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_MIN_CONTEXT_RESTORE

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_min_context_restore_req

.. sysfwapistruct:: tisci_msg_min_context_restore_resp

.. _pm_lpm_msg_set_device_constraint:

TISCI_MSG_LPM_SET_DEVICE_CONSTRAINT
===================================

Objective
----------

Any device can set a constraint on the low power mode that the SoC can enter.

.. note::
  By setting a constraint, the device ensures that it will not be powered off
  or reset in the selected mode. Setting constraint on a device does not 
  ensure that the device will be functional in the selected low power mode.
  Exception: Setting constraint on DDR ensures that DDR will be functional in 
  the selected low power mode.
  All constraints will be auto cleared at every resume cycle.

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | No     |
+------------------------+--------+

TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_LPM_SET_DEVICE_CONSTRAINT

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_lpm_set_device_constraint_req

.. sysfwapistruct:: tisci_msg_lpm_set_device_constraint_resp

.. _pm_lpm_msg_set_latency_constraint:

TISCI_MSG_LPM_SET_LATENCY_CONSTRAINT
====================================

Objective
----------

Any host can set a constraint on the low power mode that the SoC can enter.

Latency Table
-------------

+------------------+--------------+--------------+
| Low power mode   | Minimum (ms) | Maximum (ms) |
+==================+==============+==============+
| DEEPEST          | 251          | X            |
+------------------+--------------+--------------+
| DEEP_SLEEP       | 101          | 250          |
+------------------+--------------+--------------+
| MCU_ONLY         | 10           | 100          |
+------------------+--------------+--------------+
 
In the above table, DEEPEST is DEEP_SLEEP for am62x and IO_ONLY_PLUS_DDR for 
am62ax and am62px.

.. note::
  By setting a wakeup latency constraint, the host ensures that the resume
  time from selected low power mode will be less than the constraint value.
  For deepest low power mode, there is no upper limit (maximum value) for 
  latency.
  All constraints will be auto cleared at every resume cycle.

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | No     |
+------------------------+--------+

TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_LPM_SET_LATENCY_CONSTRAINT

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_lpm_set_latency_constraint_req

.. sysfwapistruct:: tisci_msg_lpm_set_latency_constraint_resp

.. _pm_lpm_msg_get_device_constraint:

TISCI_MSG_LPM_GET_DEVICE_CONSTRAINT
===================================

Objective
----------

Get the constraints set by a host on a device.

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | No     |
+------------------------+--------+

TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_LPM_GET_DEVICE_CONSTRAINT

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_lpm_get_device_constraint_req

.. sysfwapistruct:: tisci_msg_lpm_get_device_constraint_resp

.. _pm_lpm_msg_get_latency_constraint:

TISCI_MSG_LPM_GET_LATENCY_CONSTRAINT
====================================

Objective
----------

Get the low power mode resume latency value set by a host.

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | No     |
+------------------------+--------+

TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_LPM_GET_LATENCY_CONSTRAINT

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_lpm_get_latency_constraint_req

.. sysfwapistruct:: tisci_msg_lpm_get_latency_constraint_resp

.. _pm_lpm_msg_get_next_sys_mode:

TISCI_MSG_LPM_GET_NEXT_SYS_MODE
===============================

Objective
----------

Return the next (upcoming) power state of the system. It can be 
one of the following system modes.

System modes
------------

+------------------+------------+
| System mode      | Mode ID    |
+==================+============+
| DEEP_SLEEP       | 0x00       |
+------------------+------------+
| MCU_ONLY         | 0x01       |
+------------------+------------+
| IO_ONLY_PLUS_DDR | 0x02       |
+------------------+------------+
| PARTIAL_IO       | 0x03       |
+------------------+------------+
| STANDBY          | 0x04       |
+------------------+------------+
| NOT_SELECTED     | 0xFE       |
+------------------+------------+

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | No     |
+------------------------+--------+

TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_LPM_GET_NEXT_SYS_MODE

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_lpm_get_next_sys_mode_req

.. sysfwapistruct:: tisci_msg_lpm_get_next_sys_mode_resp

.. _pm_lpm_msg_get_next_host_state:

TISCI_MSG_LPM_GET_NEXT_HOST_STATE
=================================

Objective
----------

Return the next (upcoming) power state of the requesting host.

Host states
-----------

+--------------+------------+
| Host state   | State ID   |
+==============+============+
| OFF          | 0x00       |
+--------------+------------+
| ON           | 0x01       |
+--------------+------------+
| INVALID      | 0xFF       |
+--------------+------------+

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | No     |
+------------------------+--------+

TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_LPM_GET_NEXT_HOST_STATE

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_lpm_get_next_host_state_req

.. sysfwapistruct:: tisci_msg_lpm_get_next_host_state_resp

.. _pm_lpm_msg_lpm_abort:

TISCI_MSG_LPM_ABORT
===================

Objective
----------

Abort the current low power mode entry if any error occurs.

Usage
-----

+------------------------+--------+
| **Message Type**       | Normal |
+------------------------+--------+
| **Secure Queue Only?** | No     |
+------------------------+--------+

TISCI Message ID
----------------

.. sysfwapimacro:: TISCI_MSG_LPM_ABORT

Message Data Structures
-----------------------

.. sysfwapistruct:: tisci_msg_lpm_abort_req

.. sysfwapistruct:: tisci_msg_lpm_abort_resp
