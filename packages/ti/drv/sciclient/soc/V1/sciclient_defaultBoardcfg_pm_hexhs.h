/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
*  \file sciclient_defaultBoardcfg_pm_hexhs.h
*
*  \brief File containing the Binary in a C array.
*
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#ifndef SCICLIENT_DEFAULTBOARDCFG_PM_HEXHS_H_
#define SCICLIENT_DEFAULTBOARDCFG_PM_HEXHS_H_


#define SCICLIENT_BOARDCFG_PM_SIZE_IN_BYTES (1644U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define SCICLIENT_BOARDCFG_PM { \
    0x66068230U,     0x4e048230U,     0x010203a0U,     0x10140202U, \
    0x43573164U,     0xc997e664U,     0x85e51858U,     0x55aac7ceU, \
    0x30936c37U,     0x2a09060dU,     0xf7864886U,     0x0d01010dU, \
    0x81300005U,     0x300b318bU,     0x55030609U,     0x02130604U, \
    0x0b315355U,     0x03060930U,     0x0c080455U,     0x31435302U, \
    0x060d300fU,     0x07045503U,     0x6144060cU,     0x73616c6cU, \
    0x1f302131U,     0x04550306U,     0x54180c0aU,     0x73617865U, \
    0x736e4920U,     0x6d757274U,     0x73746e65U,     0x49202c2eU, \
    0x312e636eU,     0x060a300cU,     0x0b045503U,     0x4250030cU, \
    0x300f3155U,     0x5503060dU,     0x060c0304U,     0x65626c41U, \
    0x1c317472U,     0x09061a30U,     0x8648862aU,     0x09010df7U, \
    0x410d1601U,     0x7265626cU,     0x69744074U,     0x6d6f632eU, \
    0x0d171e30U,     0x33303532U,     0x34303430U,     0x37323434U, \
    0x320d175aU,     0x30343035U,     0x34343033U,     0x5a373234U, \
    0x318b8130U,     0x0609300bU,     0x06045503U,     0x53550213U, \
    0x09300b31U,     0x04550306U,     0x53020c08U,     0x300f3143U, \
    0x5503060dU,     0x060c0704U,     0x6c6c6144U,     0x21317361U, \
    0x03061f30U,     0x0c0a0455U,     0x78655418U,     0x49207361U, \
    0x7274736eU,     0x6e656d75U,     0x2c2e7374U,     0x636e4920U, \
    0x300c312eU,     0x5503060aU,     0x030c0b04U,     0x31554250U, \
    0x060d300fU,     0x03045503U,     0x6c41060cU,     0x74726562U, \
    0x1a301c31U,     0x862a0906U,     0x0df78648U,     0x16010901U, \
    0x626c410dU,     0x40747265U,     0x632e6974U,     0x82306d6fU, \
    0x0d302202U,     0x862a0906U,     0x0df78648U,     0x05010101U, \
    0x02820300U,     0x8230000fU,     0x82020a02U,     0xbf000102U, \
    0xd849ae14U,     0x6bd3727fU,     0x48ebcd23U,     0x22dc650eU, \
    0x4f0ef24dU,     0xb5edf682U,     0x7cdbddf2U,     0x596efa91U, \
    0xb6f7d5ffU,     0x8a1d04deU,     0xd995d2ccU,     0xc1c4e0d1U, \
    0xffbf50f8U,     0x22910c48U,     0x7b4c9a50U,     0x0a96f38bU, \
    0xa4b32628U,     0x55a9e0d9U,     0x3efb1a41U,     0xbf6c275bU, \
    0xaf71c0caU,     0xee22722fU,     0x62250146U,     0x04c73eadU, \
    0xb618b1f6U,     0x6e12c02cU,     0x3e9be20fU,     0xa8a0a6e5U, \
    0x41034506U,     0x1f164e17U,     0x84d674a9U,     0xa779d64eU, \
    0xa911b810U,     0x251f920eU,     0xf2b17fddU,     0x68f2b9d1U, \
    0x4b5933d8U,     0xcc777d82U,     0x23fa9cd1U,     0x8858fbb4U, \
    0xd5eacdf2U,     0x752cf216U,     0xc362fa2dU,     0xe06e09c1U, \
    0xb5e07006U,     0x62990907U,     0xe7e4d6d9U,     0x82c86d6cU, \
    0xf7935007U,     0xd1edd8e2U,     0x9ed0e35fU,     0xd95493cfU, \
    0xce5ddd5fU,     0xabf16037U,     0x7b048a14U,     0x7fbaa765U, \
    0x7c4545dfU,     0xae5ba14bU,     0x3d94c64eU,     0xd2874e8cU, \
    0xf3a43c94U,     0xf2fcda9fU,     0x0de77c36U,     0x37425aadU, \
    0xd0812af1U,     0x67a7a16eU,     0xed871e03U,     0x4a73bb00U, \
    0xa2312868U,     0x04a39a82U,     0xff87e8c1U,     0xc1aa7e45U, \
    0x053bd49fU,     0x21fd83c7U,     0x7fbdfe71U,     0x1916c938U, \
    0x03e60e52U,     0x1e1d8d33U,     0xcd1c36c9U,     0x29829d4eU, \
    0x2a9bcd88U,     0x7b5f6cbeU,     0x793ab2b2U,     0xf57d6a00U, \
    0x1e9d1aadU,     0xcf2a58cdU,     0x804ef45eU,     0xdd4f3babU, \
    0x34ded4f8U,     0xd920c4a2U,     0x852d1959U,     0x681f5e02U, \
    0xb98d4cb1U,     0x2de90611U,     0x8c58b576U,     0x6e37a850U, \
    0x836f7866U,     0x344d4630U,     0x4a18b49fU,     0x7bfabbb9U, \
    0x32d6aec5U,     0x6c848410U,     0x33809a3fU,     0xbc4dfc35U, \
    0x54606ed5U,     0x6d7ecf50U,     0xfa049780U,     0xfd200b8fU, \
    0xa12b98bdU,     0xfd59bd37U,     0xa145ec4aU,     0xc9178b09U, \
    0xb7331472U,     0x5d125e05U,     0xce1d5ae2U,     0xe1f65421U, \
    0xaa55d5eaU,     0x094deb27U,     0x824019dfU,     0x1789662eU, \
    0xb36ed965U,     0x8d4e38d6U,     0x74d61661U,     0x5f16ded4U, \
    0x42d51951U,     0xc8d283b8U,     0x69a94bdeU,     0x028db697U, \
    0x01000103U,     0x30bf81a3U,     0x0c30bc81U,     0x1d550306U, \
    0x30050413U,     0xff010103U,     0x09061230U,     0x0401062bU, \
    0x01268201U,     0x30050403U,     0x01010203U,     0x09065f30U, \
    0x0401062bU,     0x01268201U,     0x30520422U,     0x60090650U, \
    0x65014886U,     0x03020403U,     0x53804004U,     0xdd70616cU, \
    0x08dc2686U,     0xd348f11aU,     0x5dfdc29eU,     0x78c50c09U, \
    0xe74766a7U,     0x4bd33f90U,     0x33432ed0U,     0x0e7be5ecU, \
    0x6f11ff24U,     0x6f9b4243U,     0x4b8341f5U,     0xc8f00ed4U, \
    0xce3a56d3U,     0xd20fedf5U,     0x0102b854U,     0x06183002U, \
    0x01062b09U,     0x26820104U,     0x0b042301U,     0x04040930U, \
    0x00000000U,     0x30020102U,     0x5503061dU,     0x16040e1dU, \
    0xdf041404U,     0x1484b3bbU,     0xcb0c2577U,     0x7da4332eU, \
    0x1259854dU,     0x0d30e5fbU,     0x862a0906U,     0x0df78648U, \
    0x050d0101U,     0x02820300U,     0x4a3a0001U,     0x0e0077a1U, \
    0x38675fe3U,     0x3733282aU,     0x5c849d7aU,     0xfa657588U, \
    0x220df71bU,     0xe5be61c5U,     0x297e68d1U,     0xab02fca0U, \
    0x5960150dU,     0xe4c55413U,     0x8b98ffdaU,     0x3f1754c6U, \
    0x34fd3853U,     0xa9fe39c9U,     0x2d488a64U,     0x8b96ec53U, \
    0x2a6ec5adU,     0x5d98e451U,     0x919e92c9U,     0x3c6fe5cdU, \
    0xcb0ea6eaU,     0x50ea6769U,     0xc1f04555U,     0xee416ecfU, \
    0x2adbd33eU,     0x2eb7f505U,     0xe0a1be94U,     0x071b62f6U, \
    0x41517d5bU,     0x5eb118f3U,     0x7e988760U,     0xe0411ebdU, \
    0xb7eb3bd1U,     0xd86d912dU,     0x58cbcccfU,     0x1fe34c7cU, \
    0x4579d96fU,     0x3b53ca73U,     0x5cd47ee5U,     0x4ad1ad00U, \
    0xe50cbf4eU,     0x9e497842U,     0x223ceda8U,     0x1c8481fcU, \
    0x5b16325aU,     0x691d4067U,     0xcfe712c5U,     0x95f6ecc5U, \
    0x74344c46U,     0xd595bb70U,     0x653e2adbU,     0xdce4e49eU, \
    0x7d0f90e7U,     0x28969ee9U,     0xc430b7d7U,     0x31328ebcU, \
    0xc94fd509U,     0x202fbaaaU,     0x5976d0ebU,     0x1174b491U, \
    0x7bc608aeU,     0x25374deaU,     0x8dc5707dU,     0xccff605aU, \
    0x1d6c1b2dU,     0xdce1e02fU,     0xa3536039U,     0x86c35f1fU, \
    0x02e7a70dU,     0xef461c33U,     0xcb279db6U,     0x14f698faU, \
    0xc86d7351U,     0xfa8d770bU,     0x2b1f8aceU,     0x8d7ebc88U, \
    0xe9b1c6d8U,     0x9df321e8U,     0x7aa29a17U,     0x4ffe71e8U, \
    0x4a90b6c9U,     0x1d659433U,     0x29619940U,     0x7272387dU, \
    0x23b9a91fU,     0x4e9e229bU,     0x2a7a0f57U,     0x84c006d5U, \
    0xd08df53eU,     0x6619ed88U,     0xaeeb94fcU,     0xfb290719U, \
    0xcb2cc7b2U,     0x6e999752U,     0x5bc057ffU,     0x861779e2U, \
    0xb571c91cU,     0xbb50be3fU,     0xaf8b0ecfU,     0x1b6471efU, \
    0xafb9d648U,     0xa7709379U,     0x08e23d8eU,     0x981dce98U, \
    0x26b8744dU,     0xa9e7b998U,     0x83509df9U,     0x8082888eU, \
    0x903bf946U,     0x269a8ec3U,     0x6dd17d80U,     0xa47fb9b3U, \
    0x9f63044bU,     0x890ba10bU,     0xc056c0dcU,     0xc389a275U, \
    0x787d4492U,     0x009e05b1U,     0x56992d3eU,     0xea6e3dccU, \
    0x164949f8U,     0x08dbe743U,     0x47fe0ae8U,     0xbca84ad1U, \
    0x107de02aU,     0xed7f6900U,     0x0100202cU\
} /* 1644 bytes */

#endif
