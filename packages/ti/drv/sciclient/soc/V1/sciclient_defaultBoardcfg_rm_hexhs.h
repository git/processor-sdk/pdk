/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
*  \file sciclient_defaultBoardcfg_rm_hexhs.h
*
*  \brief File containing the Binary in a C array.
*
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#ifndef SCICLIENT_DEFAULTBOARDCFG_RM_HEXHS_H_
#define SCICLIENT_DEFAULTBOARDCFG_RM_HEXHS_H_


#define SCICLIENT_BOARDCFG_RM_SIZE_IN_BYTES (5849U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define SCICLIENT_BOARDCFG_RM { \
    0x67068230U,     0x4f048230U,     0x010203a0U,     0x46140202U, \
    0xefebd31dU,     0xc6c0c808U,     0x21f3aaaaU,     0xa8cc6382U, \
    0x3096d925U,     0x2a09060dU,     0xf7864886U,     0x0d01010dU, \
    0x81300005U,     0x300b318bU,     0x55030609U,     0x02130604U, \
    0x0b315355U,     0x03060930U,     0x0c080455U,     0x31435302U, \
    0x060d300fU,     0x07045503U,     0x6144060cU,     0x73616c6cU, \
    0x1f302131U,     0x04550306U,     0x54180c0aU,     0x73617865U, \
    0x736e4920U,     0x6d757274U,     0x73746e65U,     0x49202c2eU, \
    0x312e636eU,     0x060a300cU,     0x0b045503U,     0x4250030cU, \
    0x300f3155U,     0x5503060dU,     0x060c0304U,     0x65626c41U, \
    0x1c317472U,     0x09061a30U,     0x8648862aU,     0x09010df7U, \
    0x410d1601U,     0x7265626cU,     0x69744074U,     0x6d6f632eU, \
    0x0d171e30U,     0x33303532U,     0x34303430U,     0x37323434U, \
    0x320d175aU,     0x30343035U,     0x34343033U,     0x5a373234U, \
    0x318b8130U,     0x0609300bU,     0x06045503U,     0x53550213U, \
    0x09300b31U,     0x04550306U,     0x53020c08U,     0x300f3143U, \
    0x5503060dU,     0x060c0704U,     0x6c6c6144U,     0x21317361U, \
    0x03061f30U,     0x0c0a0455U,     0x78655418U,     0x49207361U, \
    0x7274736eU,     0x6e656d75U,     0x2c2e7374U,     0x636e4920U, \
    0x300c312eU,     0x5503060aU,     0x030c0b04U,     0x31554250U, \
    0x060d300fU,     0x03045503U,     0x6c41060cU,     0x74726562U, \
    0x1a301c31U,     0x862a0906U,     0x0df78648U,     0x16010901U, \
    0x626c410dU,     0x40747265U,     0x632e6974U,     0x82306d6fU, \
    0x0d302202U,     0x862a0906U,     0x0df78648U,     0x05010101U, \
    0x02820300U,     0x8230000fU,     0x82020a02U,     0xbf000102U, \
    0xd849ae14U,     0x6bd3727fU,     0x48ebcd23U,     0x22dc650eU, \
    0x4f0ef24dU,     0xb5edf682U,     0x7cdbddf2U,     0x596efa91U, \
    0xb6f7d5ffU,     0x8a1d04deU,     0xd995d2ccU,     0xc1c4e0d1U, \
    0xffbf50f8U,     0x22910c48U,     0x7b4c9a50U,     0x0a96f38bU, \
    0xa4b32628U,     0x55a9e0d9U,     0x3efb1a41U,     0xbf6c275bU, \
    0xaf71c0caU,     0xee22722fU,     0x62250146U,     0x04c73eadU, \
    0xb618b1f6U,     0x6e12c02cU,     0x3e9be20fU,     0xa8a0a6e5U, \
    0x41034506U,     0x1f164e17U,     0x84d674a9U,     0xa779d64eU, \
    0xa911b810U,     0x251f920eU,     0xf2b17fddU,     0x68f2b9d1U, \
    0x4b5933d8U,     0xcc777d82U,     0x23fa9cd1U,     0x8858fbb4U, \
    0xd5eacdf2U,     0x752cf216U,     0xc362fa2dU,     0xe06e09c1U, \
    0xb5e07006U,     0x62990907U,     0xe7e4d6d9U,     0x82c86d6cU, \
    0xf7935007U,     0xd1edd8e2U,     0x9ed0e35fU,     0xd95493cfU, \
    0xce5ddd5fU,     0xabf16037U,     0x7b048a14U,     0x7fbaa765U, \
    0x7c4545dfU,     0xae5ba14bU,     0x3d94c64eU,     0xd2874e8cU, \
    0xf3a43c94U,     0xf2fcda9fU,     0x0de77c36U,     0x37425aadU, \
    0xd0812af1U,     0x67a7a16eU,     0xed871e03U,     0x4a73bb00U, \
    0xa2312868U,     0x04a39a82U,     0xff87e8c1U,     0xc1aa7e45U, \
    0x053bd49fU,     0x21fd83c7U,     0x7fbdfe71U,     0x1916c938U, \
    0x03e60e52U,     0x1e1d8d33U,     0xcd1c36c9U,     0x29829d4eU, \
    0x2a9bcd88U,     0x7b5f6cbeU,     0x793ab2b2U,     0xf57d6a00U, \
    0x1e9d1aadU,     0xcf2a58cdU,     0x804ef45eU,     0xdd4f3babU, \
    0x34ded4f8U,     0xd920c4a2U,     0x852d1959U,     0x681f5e02U, \
    0xb98d4cb1U,     0x2de90611U,     0x8c58b576U,     0x6e37a850U, \
    0x836f7866U,     0x344d4630U,     0x4a18b49fU,     0x7bfabbb9U, \
    0x32d6aec5U,     0x6c848410U,     0x33809a3fU,     0xbc4dfc35U, \
    0x54606ed5U,     0x6d7ecf50U,     0xfa049780U,     0xfd200b8fU, \
    0xa12b98bdU,     0xfd59bd37U,     0xa145ec4aU,     0xc9178b09U, \
    0xb7331472U,     0x5d125e05U,     0xce1d5ae2U,     0xe1f65421U, \
    0xaa55d5eaU,     0x094deb27U,     0x824019dfU,     0x1789662eU, \
    0xb36ed965U,     0x8d4e38d6U,     0x74d61661U,     0x5f16ded4U, \
    0x42d51951U,     0xc8d283b8U,     0x69a94bdeU,     0x028db697U, \
    0x01000103U,     0x30c081a3U,     0x0c30bd81U,     0x1d550306U, \
    0x30050413U,     0xff010103U,     0x09061230U,     0x0401062bU, \
    0x01268201U,     0x30050403U,     0x01010203U,     0x09066030U, \
    0x0401062bU,     0x01268201U,     0x30530422U,     0x60090651U, \
    0x65014886U,     0x03020403U,     0xc8864004U,     0x3ecba2bcU, \
    0x206fa04fU,     0x7eaf1cdfU,     0x21da9adcU,     0x43aa39f1U, \
    0x45bb43c3U,     0x76e44ac6U,     0x9596dfa0U,     0x46fedfd5U, \
    0xf6d216a5U,     0x95dd4d12U,     0x2fbcac84U,     0xfd44429cU, \
    0xec38c2ecU,     0x87cdf638U,     0x02028dbdU,     0x18306e10U, \
    0x062b0906U,     0x82010401U,     0x04230126U,     0x0409300bU, \
    0x00000004U,     0x02010200U,     0x03061d30U,     0x040e1d55U, \
    0x04140416U,     0x84b3bbdfU,     0x0c257714U,     0xa4332ecbU, \
    0x59854d7dU,     0x30e5fb12U,     0x2a09060dU,     0xf7864886U, \
    0x0d01010dU,     0x82030005U,     0x8c000102U,     0xafe9eaf9U, \
    0xd52125fbU,     0x5eb33566U,     0x758bd7b2U,     0x501fc5ceU, \
    0x4ed816cdU,     0x6a86c892U,     0x817429bdU,     0x9fe1bf36U, \
    0x248253e4U,     0x175ac0ddU,     0x30dcb780U,     0x5d37bd27U, \
    0xd657b216U,     0xf1baf3caU,     0x1f096f9cU,     0x9759ab31U, \
    0x9950ff7bU,     0x12c136caU,     0xf0fa3c9fU,     0x9aeafd1aU, \
    0x2eccf7ecU,     0xda48aaceU,     0x18c352d9U,     0x92a5c3aeU, \
    0xca7fb373U,     0xd73ff689U,     0x76025696U,     0x9113b6dcU, \
    0x3771bc3fU,     0xb316418bU,     0xf11d6c2cU,     0x174b0897U, \
    0x63a199e6U,     0x81644d15U,     0xe75beb33U,     0x0ff587ddU, \
    0xf4b67ecdU,     0xba0f7242U,     0x990c69e1U,     0x7adaada0U, \
    0x66ffd6e8U,     0x5ec4a20dU,     0x19e6014bU,     0xc8c32bc8U, \
    0x84a8506eU,     0xbf26040cU,     0x125494d4U,     0x4e0934f0U, \
    0xd99cd8acU,     0x07c1fa32U,     0x6e42ed99U,     0x9c5145ebU, \
    0xf51d24acU,     0x1258448fU,     0x0d4b1091U,     0xa6ff63b7U, \
    0x9f0a39cfU,     0xdeb22af7U,     0x45aba41eU,     0x222743f4U, \
    0x47dc5d93U,     0x67330b33U,     0xcc6c42f5U,     0x196a117eU, \
    0xa7556f0eU,     0xa56dc9d1U,     0x164b71c5U,     0x23b04531U, \
    0xf6003156U,     0xda873430U,     0xd2544585U,     0xe266f9a7U, \
    0x97e7e944U,     0x353d4fd8U,     0x27ef94c9U,     0x9b3bade3U, \
    0xe5115cbbU,     0xd2ebbd19U,     0xb15ce45bU,     0x9a359459U, \
    0xc6e92e12U,     0xc398aa58U,     0x74365ed7U,     0x5796a7d8U, \
    0x5c2182cfU,     0xaca7f922U,     0x093fcf38U,     0x8ff53005U, \
    0x672d9a00U,     0xf1862dc2U,     0xe55f3676U,     0x9bed16e6U, \
    0x665bf42fU,     0xee456f9bU,     0x0b0340acU,     0x0c14be0eU, \
    0xa7aebf16U,     0x3596bc4dU,     0x5e48a852U,     0xf0017667U, \
    0xb61ed2c1U,     0xbd14f8cfU,     0x051d8359U,     0xb505f49eU, \
    0xba933fa3U,     0x58f07ee4U,     0x8351f34cU,     0x10c14f1dU, \
    0x5e58d8d7U,     0x7c7db476U,     0x7a16cb0dU,     0xeb088ac6U, \
    0x244dfbcdU,     0x7761c8d3U,     0x6a661e93U,     0xe2bee027U, \
    0xdf371bb6U,     0xfc0f1955U,     0x778a7e34U,     0xff16f11eU, \
    0xbcd5e9a0U,     0x1e40a6aeU,     0x04111934U,     0x35553e5eU, \
    0xa1cf1cfdU,     0xaa0a93a7U,     0x00541951U,     0x644c4101U, \
    0xaa2a0301U,     0xaaaaaaaaU,     0xaaaaaaaaU,     0xaaaa2a05U, \
    0xaaaaaaaaU,     0x0caaaaaaU,     0xaaaaaa2aU,     0xaaaaaaaaU, \
    0x2a0daaaaU,     0xaaaaaaaaU,     0xaaaaaaaaU,     0xaa2a15aaU, \
    0xaaaaaaaaU,     0xaaaaaaaaU,     0xaaaa2a1aU,     0xaaaaaaaaU, \
    0x1caaaaaaU,     0xaaaaaa2aU,     0xaaaaaaaaU,     0x2a23aaaaU, \
    0xaaaaaaaaU,     0xaaaaaaaaU,     0xaa2a25aaU,     0xaaaaaaaaU, \
    0xaaaaaaaaU,     0xaaaa2a28U,     0xaaaaaaaaU,     0x2aaaaaaaU, \
    0xaaaaaa2aU,     0xaaaaaaaaU,     0x0000aaaaU,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x087b2500U,     0x000d1000U,     0x5d000400U,     0x1a1e4000U, \
    0x5d000400U,     0x1c1e8000U,     0x20000000U,     0x801ec000U, \
    0x20000000U,     0x03200000U,     0x20002000U,     0x05200000U, \
    0x18000000U,     0x03208000U,     0x18001800U,     0x05208000U, \
    0x08000000U,     0x0320c000U,     0x08000800U,     0x0520c000U, \
    0x04001000U,     0x2820c000U,     0x04001400U,     0x2a20c000U, \
    0x04001800U,     0x2320c000U,     0x04001c00U,     0x2520c000U, \
    0x04002000U,     0x1a20c000U,     0x04002400U,     0x1c20c000U, \
    0x0c002800U,     0x0c20c000U,     0x0c003400U,     0x0d20c000U, \
    0x80000000U,     0x23218000U,     0x80008000U,     0x25218000U, \
    0x80000000U,     0x2821c000U,     0x80008000U,     0x2a21c000U, \
    0x30000000U,     0x80220000U,     0x08000000U,     0x03224000U, \
    0x08000800U,     0x05224000U,     0x06001000U,     0x0c224000U, \
    0x06001600U,     0x0d224000U,     0x02001c00U,     0x23224000U, \
    0x02001e00U,     0x25224000U,     0x40000000U,     0x8033ca00U, \
    0x00500000U,     0x8033cd04U,     0x40000000U,     0x80340a00U, \
    0x00580000U,     0x80340d04U,     0x56002600U,     0x0c344a00U, \
    0x20007c00U,     0x0d344a00U,     0x0c009c00U,     0x28344a00U, \
    0x0c00a800U,     0x2a344a00U,     0x0c00b400U,     0x15344a00U, \
    0x0c00c000U,     0x1a344a00U,     0x0c00cc00U,     0x1c344a00U, \
    0x1c00d800U,     0x23344a00U,     0x0800f400U,     0x25344a00U, \
    0x0400fc00U,     0x80344a00U,     0x00002600U,     0x0c344d04U, \
    0x00042600U,     0x0d344d02U,     0x20062600U,     0x03344d00U, \
    0x20064600U,     0x05344d00U,     0x00066600U,     0x28344d01U, \
    0x00076600U,     0x2a344d01U,     0x00086600U,     0x15344d01U, \
    0x00096600U,     0x1a344d01U,     0x000a6600U,     0x1c344d01U, \
    0x000b6600U,     0x23344d02U,     0x000d6600U,     0x25344d01U, \
    0x9a0e6600U,     0x80344d03U,     0x04000000U,     0x0c348000U, \
    0x04000400U,     0x0d348000U,     0x04000800U,     0x03348000U, \
    0x04000c00U,     0x05348000U,     0x04001000U,     0x28348000U, \
    0x04001400U,     0x2a348000U,     0x04001800U,     0x15348000U, \
    0x04001c00U,     0x1a348000U,     0x04002000U,     0x1c348000U, \
    0x0c002400U,     0x23348000U,     0x04003000U,     0x25348000U, \
    0x0c003400U,     0x80348000U,     0x01000000U,     0x8034c000U, \
    0x9601b800U,     0x0c34c100U,     0x28024e00U,     0x0d34c100U, \
    0x06027600U,     0x0334c100U,     0x06027c00U,     0x0534c100U, \
    0x0a028200U,     0x2834c100U,     0x0a028c00U,     0x2a34c100U, \
    0x20029600U,     0x1534c100U,     0x2602b600U,     0x1a34c100U, \
    0x0c02dc00U,     0x1c34c100U,     0xb602e800U,     0x2334c100U, \
    0x28039e00U,     0x2534c100U,     0x0803c600U,     0x8034c100U, \
    0x08013c00U,     0x0c34c200U,     0x02014400U,     0x0334c200U, \
    0x00014400U,     0x0d34c200U,     0x02014600U,     0x0534c200U, \
    0x02014800U,     0x2834c200U,     0x02014a00U,     0x2a34c200U, \
    0x02014c00U,     0x1534c200U,     0x08014e00U,     0x1a34c200U, \
    0x02015600U,     0x1c34c200U,     0x04015800U,     0x2334c200U, \
    0x01015c00U,     0x2534c200U,     0x2f015d00U,     0x0c34c200U, \
    0x01018c00U,     0x0d34c200U,     0x04018d00U,     0x2834c200U, \
    0x04019100U,     0x2a34c200U,     0x04019500U,     0x1534c200U, \
    0x08019900U,     0x1a34c200U,     0x0601a100U,     0x1c34c200U, \
    0x1001a700U,     0x2334c200U,     0x0101b700U,     0x2534c200U, \
    0x08001000U,     0x0c34c300U,     0x02001800U,     0x0334c300U, \
    0x00001800U,     0x0d34c300U,     0x02001a00U,     0x0534c300U, \
    0x02001c00U,     0x2834c300U,     0x02001e00U,     0x2a34c300U, \
    0x02002000U,     0x1534c300U,     0x08002200U,     0x1a34c300U, \
    0x02002a00U,     0x1c34c300U,     0x04002c00U,     0x2334c300U, \
    0x01003000U,     0x2534c300U,     0x2f003100U,     0x0c34c300U, \
    0x01006000U,     0x0d34c300U,     0x04006100U,     0x2834c300U, \
    0x04006500U,     0x2a34c300U,     0x04006900U,     0x1534c300U, \
    0x08006d00U,     0x1a34c300U,     0x06007500U,     0x1c34c300U, \
    0x0a007b00U,     0x2334c300U,     0x06008500U,     0x2534c300U, \
    0x01008b00U,     0x8034c300U,     0x10008c00U,     0x1534c400U, \
    0x06009c00U,     0x1a34c400U,     0x0600a200U,     0x1c34c400U, \
    0x0200a800U,     0x2334c400U,     0x0200aa00U,     0x2534c400U, \
    0x6000ac00U,     0x2334c400U,     0x20010c00U,     0x2534c400U, \
    0x00013000U,     0x0c34c500U,     0x04013000U,     0x0c34c500U, \
    0x00013000U,     0x2334c500U,     0x06013400U,     0x2334c500U, \
    0x02013a00U,     0x8034c500U,     0x00012c00U,     0x0c34c600U, \
    0x02012c00U,     0x0c34c600U,     0x00012c00U,     0x2334c600U, \
    0x02012e00U,     0x2334c600U,     0x00000400U,     0x0c34c700U, \
    0x04000400U,     0x0c34c700U,     0x00000400U,     0x2334c700U, \
    0x06000800U,     0x2334c700U,     0x02000e00U,     0x8034c700U, \
    0x00000000U,     0x0c34c800U,     0x02000000U,     0x0c34c800U, \
    0x00000000U,     0x2334c800U,     0x02000200U,     0x2334c800U, \
    0x05000200U,     0x0c34ca00U,     0x01000700U,     0x0d34ca00U, \
    0x03000000U,     0x0c34cb00U,     0x02000300U,     0x0d34cb00U, \
    0x01000500U,     0x0334cb00U,     0x01000600U,     0x0534cb00U, \
    0x03000700U,     0x2834cb00U,     0x03000a00U,     0x2a34cb00U, \
    0x03000d00U,     0x1534cb00U,     0x03001000U,     0x1a34cb00U, \
    0x03001300U,     0x1c34cb00U,     0x06001600U,     0x2334cb00U, \
    0x03001c00U,     0x2534cb00U,     0x01001f00U,     0x8034cb00U, \
    0x10008c00U,     0x0c350000U,     0x10009c00U,     0x0d350000U, \
    0x8000ac00U,     0x80350000U,     0x01000000U,     0x80350100U, \
    0x00c00000U,     0x80350204U,     0x01000000U,     0x80350300U, \
    0x08001000U,     0x0c350a00U,     0x02001800U,     0x03350a00U, \
    0x00001800U,     0x0d350a00U,     0x02001a00U,     0x05350a00U, \
    0x02001c00U,     0x28350a00U,     0x02001e00U,     0x2a350a00U, \
    0x02002000U,     0x15350a00U,     0x08002200U,     0x1a350a00U, \
    0x02002a00U,     0x1c350a00U,     0x04002c00U,     0x23350a00U, \
    0x01003000U,     0x25350a00U,     0x2f003100U,     0x0c350a00U, \
    0x01006000U,     0x0d350a00U,     0x04006100U,     0x28350a00U, \
    0x04006500U,     0x2a350a00U,     0x04006900U,     0x15350a00U, \
    0x08006d00U,     0x1a350a00U,     0x06007500U,     0x1c350a00U, \
    0x10007b00U,     0x23350a00U,     0x01008b00U,     0x25350a00U, \
    0x00000400U,     0x0c350b00U,     0x04000400U,     0x0c350b00U, \
    0x00000400U,     0x23350b00U,     0x06000800U,     0x23350b00U, \
    0x02000e00U,     0x80350b00U,     0x00000000U,     0x0c350c00U, \
    0x02000000U,     0x0c350c00U,     0x00000000U,     0x23350c00U, \
    0x02000200U,     0x23350c00U,     0x08001000U,     0x0c350d00U, \
    0x02001800U,     0x03350d00U,     0x00001800U,     0x0d350d00U, \
    0x02001a00U,     0x05350d00U,     0x02001c00U,     0x28350d00U, \
    0x02001e00U,     0x2a350d00U,     0x02002000U,     0x15350d00U, \
    0x08002200U,     0x1a350d00U,     0x02002a00U,     0x1c350d00U, \
    0x04002c00U,     0x23350d00U,     0x01003000U,     0x25350d00U, \
    0x2f003100U,     0x0c350d00U,     0x01006000U,     0x0d350d00U, \
    0x04006100U,     0x28350d00U,     0x04006500U,     0x2a350d00U, \
    0x04006900U,     0x15350d00U,     0x08006d00U,     0x1a350d00U, \
    0x06007500U,     0x1c350d00U,     0x0a007b00U,     0x23350d00U, \
    0x06008500U,     0x25350d00U,     0x01008b00U,     0x80350d00U, \
    0x10008c00U,     0x15350e00U,     0x06009c00U,     0x1a350e00U, \
    0x0600a200U,     0x1c350e00U,     0x0200a800U,     0x23350e00U, \
    0x0200aa00U,     0x25350e00U,     0x6000ac00U,     0x23350e00U, \
    0x20010c00U,     0x25350e00U,     0x00000400U,     0x0c350f00U, \
    0x04000400U,     0x0c350f00U,     0x00000400U,     0x23350f00U, \
    0x06000800U,     0x23350f00U,     0x02000e00U,     0x80350f00U, \
    0x00000000U,     0x0c351000U,     0x02000000U,     0x0c351000U, \
    0x00000000U,     0x23351000U,     0x02000200U,     0x23351000U, \
    0x64000a00U,     0x0c354000U,     0x20006e00U,     0x0d354000U, \
    0x2e008e00U,     0x15354000U,     0x1c00c400U,     0x23354000U, \
    0x1c00e400U,     0x25354000U,     0x1c010400U,     0x28354000U, \
    0x1c012400U,     0x2a354000U,     0x18014000U,     0x1a354000U, \
    0x18016000U,     0x1c354000U,     0x04019000U,     0x03354000U, \
    0x04019400U,     0x05354000U,     0x20001000U,     0x0c3a4a00U, \
    0x10003000U,     0x0d3a4a00U,     0x40004000U,     0x033a4a00U, \
    0x04008000U,     0x053a4a00U,     0x10008400U,     0x283a4a00U, \
    0x10009400U,     0x2a3a4a00U,     0x0800a400U,     0x153a4a00U, \
    0x0800ac00U,     0x1a3a4a00U,     0x0800b400U,     0x1c3a4a00U, \
    0x1800bc00U,     0x233a4a00U,     0x0800d400U,     0x253a4a00U, \
    0x2400dc00U,     0x803a4a00U,     0x80401000U,     0x0c3a4d00U, \
    0x80409000U,     0x0d3a4d00U,     0x00411000U,     0x033a4d01U, \
    0x40421000U,     0x053a4d00U,     0x80425000U,     0x283a4d00U, \
    0x8042d000U,     0x2a3a4d00U,     0x40435000U,     0x153a4d00U, \
    0x40439000U,     0x1a3a4d00U,     0x4043d000U,     0x1c3a4d00U, \
    0x80441000U,     0x233a4d00U,     0x80449000U,     0x253a4d00U, \
    0xf0451000U,     0x803a4d00U,     0x04000100U,     0x0c3a8000U, \
    0x04000500U,     0x0d3a8000U,     0x04000900U,     0x033a8000U, \
    0x04000d00U,     0x053a8000U,     0x04001100U,     0x283a8000U, \
    0x04001500U,     0x2a3a8000U,     0x04001900U,     0x153a8000U, \
    0x04001d00U,     0x1a3a8000U,     0x04002100U,     0x1c3a8000U, \
    0x10002500U,     0x233a8000U,     0x04003500U,     0x253a8000U, \
    0x07003900U,     0x803a8000U,     0x01000000U,     0x803ac000U, \
    0x14006000U,     0x0c3ac100U,     0x08007400U,     0x0d3ac100U, \
    0x20007c00U,     0x033ac100U,     0x0c009c00U,     0x053ac100U, \
    0x0800a800U,     0x283ac100U,     0x0800b000U,     0x2a3ac100U, \
    0x0800b800U,     0x153ac100U,     0x0800c000U,     0x1a3ac100U, \
    0x0800c800U,     0x1c3ac100U,     0x1000d000U,     0x233ac100U, \
    0x0800e000U,     0x253ac100U,     0x1400e800U,     0x803ac100U, \
    0x04003200U,     0x0c3ac200U,     0x02003600U,     0x033ac200U, \
    0x00003600U,     0x0d3ac200U,     0x00003800U,     0x053ac200U, \
    0x01003800U,     0x283ac200U,     0x01003900U,     0x2a3ac200U, \
    0x01003a00U,     0x153ac200U,     0x01003b00U,     0x1a3ac200U, \
    0x01003c00U,     0x1c3ac200U,     0x01003d00U,     0x233ac200U, \
    0x01003e00U,     0x253ac200U,     0x09003f00U,     0x0c3ac200U, \
    0x06004800U,     0x0d3ac200U,     0x03004e00U,     0x033ac200U, \
    0x02005100U,     0x053ac200U,     0x01005300U,     0x283ac200U, \
    0x01005400U,     0x2a3ac200U,     0x01005500U,     0x153ac200U, \
    0x01005600U,     0x1a3ac200U,     0x01005700U,     0x1c3ac200U, \
    0x02005800U,     0x233ac200U,     0x01005a00U,     0x253ac200U, \
    0x02005b00U,     0x803ac200U,     0x04000200U,     0x0c3ac300U, \
    0x02000600U,     0x033ac300U,     0x00000600U,     0x0d3ac300U, \
    0x00000800U,     0x053ac300U,     0x01000800U,     0x283ac300U, \
    0x01000900U,     0x2a3ac300U,     0x01000a00U,     0x153ac300U, \
    0x01000b00U,     0x1a3ac300U,     0x01000c00U,     0x1c3ac300U, \
    0x01000d00U,     0x233ac300U,     0x01000e00U,     0x253ac300U, \
    0x09000f00U,     0x0c3ac300U,     0x06001800U,     0x0d3ac300U, \
    0x03001e00U,     0x033ac300U,     0x02002100U,     0x053ac300U, \
    0x01002300U,     0x283ac300U,     0x01002400U,     0x2a3ac300U, \
    0x01002500U,     0x153ac300U,     0x01002600U,     0x1a3ac300U, \
    0x01002700U,     0x1c3ac300U,     0x02002800U,     0x233ac300U, \
    0x01002a00U,     0x253ac300U,     0x03002b00U,     0x803ac300U, \
    0x00003000U,     0x033ac500U,     0x02003000U,     0x033ac500U, \
    0x00000000U,     0x033ac700U,     0x02000000U,     0x033ac700U, \
    0x05000200U,     0x0c3aca00U,     0x01000700U,     0x0d3aca00U, \
    0x03000000U,     0x0c3acb00U,     0x02000300U,     0x0d3acb00U, \
    0x03000500U,     0x033acb00U,     0x03000800U,     0x053acb00U, \
    0x03000b00U,     0x283acb00U,     0x03000e00U,     0x2a3acb00U, \
    0x03001100U,     0x153acb00U,     0x03001400U,     0x1a3acb00U, \
    0x03001700U,     0x1c3acb00U,     0x03001a00U,     0x233acb00U, \
    0x03001d00U,     0x253acb00U,     0x08003000U,     0x0c3b0000U, \
    0x04003800U,     0x0d3b0000U,     0x08003c00U,     0x033b0000U, \
    0x04004400U,     0x053b0000U,     0x04004800U,     0x283b0000U, \
    0x04004c00U,     0x2a3b0000U,     0x08005000U,     0x233b0000U, \
    0x04005800U,     0x253b0000U,     0x04005c00U,     0x803b0000U, \
    0x01000000U,     0x803b0100U,     0x00dc0000U,     0x803b0201U, \
    0x01000000U,     0x803b0300U,     0x04000200U,     0x0c3b0a00U, \
    0x02000600U,     0x033b0a00U,     0x00000600U,     0x0d3b0a00U, \
    0x00000800U,     0x053b0a00U,     0x01000800U,     0x283b0a00U, \
    0x01000900U,     0x2a3b0a00U,     0x01000a00U,     0x153b0a00U, \
    0x01000b00U,     0x1a3b0a00U,     0x01000c00U,     0x1c3b0a00U, \
    0x01000d00U,     0x233b0a00U,     0x01000e00U,     0x253b0a00U, \
    0x09000f00U,     0x0c3b0a00U,     0x06001800U,     0x0d3b0a00U, \
    0x03001e00U,     0x033b0a00U,     0x02002100U,     0x053b0a00U, \
    0x01002300U,     0x283b0a00U,     0x01002400U,     0x2a3b0a00U, \
    0x01002500U,     0x153b0a00U,     0x01002600U,     0x1a3b0a00U, \
    0x01002700U,     0x1c3b0a00U,     0x02002800U,     0x233b0a00U, \
    0x01002a00U,     0x253b0a00U,     0x02002b00U,     0x803b0a00U, \
    0x00000000U,     0x033b0b00U,     0x02000000U,     0x033b0b00U, \
    0x04000200U,     0x0c3b0d00U,     0x02000600U,     0x033b0d00U, \
    0x00000600U,     0x0d3b0d00U,     0x00000800U,     0x053b0d00U, \
    0x01000800U,     0x283b0d00U,     0x01000900U,     0x2a3b0d00U, \
    0x01000a00U,     0x153b0d00U,     0x01000b00U,     0x1a3b0d00U, \
    0x01000c00U,     0x1c3b0d00U,     0x01000d00U,     0x233b0d00U, \
    0x01000e00U,     0x253b0d00U,     0x09000f00U,     0x0c3b0d00U, \
    0x06001800U,     0x0d3b0d00U,     0x03001e00U,     0x033b0d00U, \
    0x02002100U,     0x053b0d00U,     0x01002300U,     0x283b0d00U, \
    0x01002400U,     0x2a3b0d00U,     0x01002500U,     0x153b0d00U, \
    0x01002600U,     0x1a3b0d00U,     0x01002700U,     0x1c3b0d00U, \
    0x02002800U,     0x233b0d00U,     0x01002a00U,     0x253b0d00U, \
    0x03002b00U,     0x803b0d00U,     0x00000000U,     0x033b0f00U, \
    0x02000000U,     0x033b0f00U,     0x14000c00U,     0x033b4000U, \
    0x1c002400U,     0x053b4000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U,     0x00000000U, \
    0x00000000U,     0x00000000U,     0x00000000U\
} /* 5849 bytes */

#endif
