/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
*  \file sciclient_defaultBoardcfg_hexhs.h
*
*  \brief File containing the Binary in a C array.
*
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#ifndef SCICLIENT_DEFAULTBOARDCFG_HEXHS_H_
#define SCICLIENT_DEFAULTBOARDCFG_HEXHS_H_


#define SCICLIENT_BOARDCFG_SIZE_IN_BYTES (1671U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define SCICLIENT_BOARDCFG { \
    0x66068230U,     0x4e048230U,     0x010203a0U,     0x56140202U, \
    0x3a94a4dfU,     0x962d9937U,     0xa4204c1fU,     0x92bd00acU, \
    0x30bc1615U,     0x2a09060dU,     0xf7864886U,     0x0d01010dU, \
    0x81300005U,     0x300b318bU,     0x55030609U,     0x02130604U, \
    0x0b315355U,     0x03060930U,     0x0c080455U,     0x31435302U, \
    0x060d300fU,     0x07045503U,     0x6144060cU,     0x73616c6cU, \
    0x1f302131U,     0x04550306U,     0x54180c0aU,     0x73617865U, \
    0x736e4920U,     0x6d757274U,     0x73746e65U,     0x49202c2eU, \
    0x312e636eU,     0x060a300cU,     0x0b045503U,     0x4250030cU, \
    0x300f3155U,     0x5503060dU,     0x060c0304U,     0x65626c41U, \
    0x1c317472U,     0x09061a30U,     0x8648862aU,     0x09010df7U, \
    0x410d1601U,     0x7265626cU,     0x69744074U,     0x6d6f632eU, \
    0x0d171e30U,     0x33303532U,     0x34303430U,     0x32303634U, \
    0x320d175aU,     0x30343035U,     0x34343033U,     0x5a323036U, \
    0x318b8130U,     0x0609300bU,     0x06045503U,     0x53550213U, \
    0x09300b31U,     0x04550306U,     0x53020c08U,     0x300f3143U, \
    0x5503060dU,     0x060c0704U,     0x6c6c6144U,     0x21317361U, \
    0x03061f30U,     0x0c0a0455U,     0x78655418U,     0x49207361U, \
    0x7274736eU,     0x6e656d75U,     0x2c2e7374U,     0x636e4920U, \
    0x300c312eU,     0x5503060aU,     0x030c0b04U,     0x31554250U, \
    0x060d300fU,     0x03045503U,     0x6c41060cU,     0x74726562U, \
    0x1a301c31U,     0x862a0906U,     0x0df78648U,     0x16010901U, \
    0x626c410dU,     0x40747265U,     0x632e6974U,     0x82306d6fU, \
    0x0d302202U,     0x862a0906U,     0x0df78648U,     0x05010101U, \
    0x02820300U,     0x8230000fU,     0x82020a02U,     0xbf000102U, \
    0xd849ae14U,     0x6bd3727fU,     0x48ebcd23U,     0x22dc650eU, \
    0x4f0ef24dU,     0xb5edf682U,     0x7cdbddf2U,     0x596efa91U, \
    0xb6f7d5ffU,     0x8a1d04deU,     0xd995d2ccU,     0xc1c4e0d1U, \
    0xffbf50f8U,     0x22910c48U,     0x7b4c9a50U,     0x0a96f38bU, \
    0xa4b32628U,     0x55a9e0d9U,     0x3efb1a41U,     0xbf6c275bU, \
    0xaf71c0caU,     0xee22722fU,     0x62250146U,     0x04c73eadU, \
    0xb618b1f6U,     0x6e12c02cU,     0x3e9be20fU,     0xa8a0a6e5U, \
    0x41034506U,     0x1f164e17U,     0x84d674a9U,     0xa779d64eU, \
    0xa911b810U,     0x251f920eU,     0xf2b17fddU,     0x68f2b9d1U, \
    0x4b5933d8U,     0xcc777d82U,     0x23fa9cd1U,     0x8858fbb4U, \
    0xd5eacdf2U,     0x752cf216U,     0xc362fa2dU,     0xe06e09c1U, \
    0xb5e07006U,     0x62990907U,     0xe7e4d6d9U,     0x82c86d6cU, \
    0xf7935007U,     0xd1edd8e2U,     0x9ed0e35fU,     0xd95493cfU, \
    0xce5ddd5fU,     0xabf16037U,     0x7b048a14U,     0x7fbaa765U, \
    0x7c4545dfU,     0xae5ba14bU,     0x3d94c64eU,     0xd2874e8cU, \
    0xf3a43c94U,     0xf2fcda9fU,     0x0de77c36U,     0x37425aadU, \
    0xd0812af1U,     0x67a7a16eU,     0xed871e03U,     0x4a73bb00U, \
    0xa2312868U,     0x04a39a82U,     0xff87e8c1U,     0xc1aa7e45U, \
    0x053bd49fU,     0x21fd83c7U,     0x7fbdfe71U,     0x1916c938U, \
    0x03e60e52U,     0x1e1d8d33U,     0xcd1c36c9U,     0x29829d4eU, \
    0x2a9bcd88U,     0x7b5f6cbeU,     0x793ab2b2U,     0xf57d6a00U, \
    0x1e9d1aadU,     0xcf2a58cdU,     0x804ef45eU,     0xdd4f3babU, \
    0x34ded4f8U,     0xd920c4a2U,     0x852d1959U,     0x681f5e02U, \
    0xb98d4cb1U,     0x2de90611U,     0x8c58b576U,     0x6e37a850U, \
    0x836f7866U,     0x344d4630U,     0x4a18b49fU,     0x7bfabbb9U, \
    0x32d6aec5U,     0x6c848410U,     0x33809a3fU,     0xbc4dfc35U, \
    0x54606ed5U,     0x6d7ecf50U,     0xfa049780U,     0xfd200b8fU, \
    0xa12b98bdU,     0xfd59bd37U,     0xa145ec4aU,     0xc9178b09U, \
    0xb7331472U,     0x5d125e05U,     0xce1d5ae2U,     0xe1f65421U, \
    0xaa55d5eaU,     0x094deb27U,     0x824019dfU,     0x1789662eU, \
    0xb36ed965U,     0x8d4e38d6U,     0x74d61661U,     0x5f16ded4U, \
    0x42d51951U,     0xc8d283b8U,     0x69a94bdeU,     0x028db697U, \
    0x01000103U,     0x30bf81a3U,     0x0c30bc81U,     0x1d550306U, \
    0x30050413U,     0xff010103U,     0x09061230U,     0x0401062bU, \
    0x01268201U,     0x30050403U,     0x01010203U,     0x09065f30U, \
    0x0401062bU,     0x01268201U,     0x30520422U,     0x60090650U, \
    0x65014886U,     0x03020403U,     0xf1ee4004U,     0x799a0dcdU, \
    0x1c6d191fU,     0x18041929U,     0xc8d8ba6cU,     0x461108eeU, \
    0xd5c3acc2U,     0xa32ddbf1U,     0x0cdebaaaU,     0x5cf32f18U, \
    0x88d85746U,     0xd0374215U,     0xabf6972aU,     0x5922342dU, \
    0x0594b47aU,     0x2658f731U,     0x01025708U,     0x0618301dU, \
    0x01062b09U,     0x26820104U,     0x0b042301U,     0x04040930U, \
    0x00000000U,     0x30020102U,     0x5503061dU,     0x16040e1dU, \
    0xdf041404U,     0x1484b3bbU,     0xcb0c2577U,     0x7da4332eU, \
    0x1259854dU,     0x0d30e5fbU,     0x862a0906U,     0x0df78648U, \
    0x050d0101U,     0x02820300U,     0x14500001U,     0xc8eaca55U, \
    0x1910a635U,     0x13565ca7U,     0xb8662f0fU,     0x1a628ee4U, \
    0x6be4ca6fU,     0xab569954U,     0x669232aeU,     0xb2d9e2e0U, \
    0x248d37c3U,     0x1a53bf36U,     0x65f46147U,     0x846f8ee4U, \
    0x1b039718U,     0x309d9178U,     0x1b3a8f9eU,     0x216d0e46U, \
    0x0b13e50fU,     0x468cbc02U,     0x25f18dfcU,     0x989e3ccbU, \
    0xe3801facU,     0x7fcec255U,     0x97856b34U,     0x6c839630U, \
    0x4fbbed95U,     0x6b92e7f2U,     0x7536b534U,     0x248fe788U, \
    0x5a1994f7U,     0xa54e04abU,     0xc873425cU,     0xf46ad649U, \
    0x1de51adbU,     0xaf4bd2efU,     0x7b4b88c9U,     0x7a4707a2U, \
    0xa99acbb3U,     0xb34fc968U,     0xd30ecaf5U,     0xd65e71f1U, \
    0x74b3ae28U,     0x82709dd8U,     0x5911230bU,     0x3f8aa46dU, \
    0x653da6b3U,     0x2181045bU,     0x496cdc96U,     0xc055c3a5U, \
    0x1cec005cU,     0x35be8a72U,     0xe5868d22U,     0x849b7630U, \
    0x0c614fe9U,     0xb2ecb277U,     0xc9ea175aU,     0xcc38e4b7U, \
    0x46d37647U,     0x1d559f4cU,     0xde4e86c2U,     0x56e5bf6dU, \
    0xed791954U,     0xcb46332aU,     0x8cd8a523U,     0x3bc12869U, \
    0x1b44d8f6U,     0x56f8cbffU,     0x2896762dU,     0x809a86faU, \
    0x4225f8c5U,     0x933749d1U,     0x3ec8aff2U,     0xe0a137c5U, \
    0x8100da77U,     0x396b0698U,     0xb56a00fcU,     0x136b104fU, \
    0x0325d637U,     0x8b30eaafU,     0x7d65430fU,     0x21279e10U, \
    0x08a8ab9bU,     0x1765b100U,     0xb8badff1U,     0xd2c619f7U, \
    0x663dc9cfU,     0x42775ffeU,     0xe3e9d384U,     0x947f487fU, \
    0xf9d8e369U,     0xe33f9423U,     0x6a83da82U,     0xd3aeacccU, \
    0x1f4980e8U,     0x9c8fca00U,     0xdbc76661U,     0xe5b5f238U, \
    0xfa4bf734U,     0xa83b2cd8U,     0xbd1dbb4eU,     0xc9cfe5c3U, \
    0x6c53a382U,     0xd6cb1c25U,     0x0dc79e6cU,     0x0eada58aU, \
    0xe076876bU,     0xf9bc3706U,     0xa055dc62U,     0x0ceaefbbU, \
    0x150f2abcU,     0xf472c37aU,     0x6e93bf71U,     0x5087ee59U, \
    0x9ace3971U,     0xe142e94eU,     0x75fc32a7U,     0x300e2da5U, \
    0x7c4e77d7U,     0xcc2317deU,     0xb5b20d5bU,     0x9a556683U, \
    0x3106fc8dU,     0x924efe55U,     0xa270d4b7U,     0x8354e478U, \
    0x53595abdU,     0x30970e73U,     0x0100abedU,     0x0007c1d3U, \
    0x0700045aU,     0x01000712U,     0xa5c30001U,     0x0c000005U, \
    0x00000802U,     0x00000000U\
} /* 1671 bytes */

#endif
