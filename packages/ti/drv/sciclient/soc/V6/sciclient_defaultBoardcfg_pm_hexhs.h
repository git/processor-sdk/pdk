/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
*  \file sciclient_defaultBoardcfg_pm_hexhs.h
*
*  \brief File containing the Binary in a C array.
*
*/

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#ifndef SCICLIENT_DEFAULTBOARDCFG_PM_HEXHS_H_
#define SCICLIENT_DEFAULTBOARDCFG_PM_HEXHS_H_


#define SCICLIENT_BOARDCFG_PM_SIZE_IN_BYTES (1644U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define SCICLIENT_BOARDCFG_PM { \
    0x66068230U,     0x4e048230U,     0x010203a0U,     0x47140202U, \
    0x58d0770dU,     0x506ae24cU,     0xd7549fe8U,     0x6242b2c6U, \
    0x30e265dcU,     0x2a09060dU,     0xf7864886U,     0x0d01010dU, \
    0x81300005U,     0x300b318bU,     0x55030609U,     0x02130604U, \
    0x0b315355U,     0x03060930U,     0x0c080455U,     0x31435302U, \
    0x060d300fU,     0x07045503U,     0x6144060cU,     0x73616c6cU, \
    0x1f302131U,     0x04550306U,     0x54180c0aU,     0x73617865U, \
    0x736e4920U,     0x6d757274U,     0x73746e65U,     0x49202c2eU, \
    0x312e636eU,     0x060a300cU,     0x0b045503U,     0x4250030cU, \
    0x300f3155U,     0x5503060dU,     0x060c0304U,     0x65626c41U, \
    0x1c317472U,     0x09061a30U,     0x8648862aU,     0x09010df7U, \
    0x410d1601U,     0x7265626cU,     0x69744074U,     0x6d6f632eU, \
    0x0d171e30U,     0x33303532U,     0x34303430U,     0x32303634U, \
    0x320d175aU,     0x30343035U,     0x34343033U,     0x5a323036U, \
    0x318b8130U,     0x0609300bU,     0x06045503U,     0x53550213U, \
    0x09300b31U,     0x04550306U,     0x53020c08U,     0x300f3143U, \
    0x5503060dU,     0x060c0704U,     0x6c6c6144U,     0x21317361U, \
    0x03061f30U,     0x0c0a0455U,     0x78655418U,     0x49207361U, \
    0x7274736eU,     0x6e656d75U,     0x2c2e7374U,     0x636e4920U, \
    0x300c312eU,     0x5503060aU,     0x030c0b04U,     0x31554250U, \
    0x060d300fU,     0x03045503U,     0x6c41060cU,     0x74726562U, \
    0x1a301c31U,     0x862a0906U,     0x0df78648U,     0x16010901U, \
    0x626c410dU,     0x40747265U,     0x632e6974U,     0x82306d6fU, \
    0x0d302202U,     0x862a0906U,     0x0df78648U,     0x05010101U, \
    0x02820300U,     0x8230000fU,     0x82020a02U,     0xbf000102U, \
    0xd849ae14U,     0x6bd3727fU,     0x48ebcd23U,     0x22dc650eU, \
    0x4f0ef24dU,     0xb5edf682U,     0x7cdbddf2U,     0x596efa91U, \
    0xb6f7d5ffU,     0x8a1d04deU,     0xd995d2ccU,     0xc1c4e0d1U, \
    0xffbf50f8U,     0x22910c48U,     0x7b4c9a50U,     0x0a96f38bU, \
    0xa4b32628U,     0x55a9e0d9U,     0x3efb1a41U,     0xbf6c275bU, \
    0xaf71c0caU,     0xee22722fU,     0x62250146U,     0x04c73eadU, \
    0xb618b1f6U,     0x6e12c02cU,     0x3e9be20fU,     0xa8a0a6e5U, \
    0x41034506U,     0x1f164e17U,     0x84d674a9U,     0xa779d64eU, \
    0xa911b810U,     0x251f920eU,     0xf2b17fddU,     0x68f2b9d1U, \
    0x4b5933d8U,     0xcc777d82U,     0x23fa9cd1U,     0x8858fbb4U, \
    0xd5eacdf2U,     0x752cf216U,     0xc362fa2dU,     0xe06e09c1U, \
    0xb5e07006U,     0x62990907U,     0xe7e4d6d9U,     0x82c86d6cU, \
    0xf7935007U,     0xd1edd8e2U,     0x9ed0e35fU,     0xd95493cfU, \
    0xce5ddd5fU,     0xabf16037U,     0x7b048a14U,     0x7fbaa765U, \
    0x7c4545dfU,     0xae5ba14bU,     0x3d94c64eU,     0xd2874e8cU, \
    0xf3a43c94U,     0xf2fcda9fU,     0x0de77c36U,     0x37425aadU, \
    0xd0812af1U,     0x67a7a16eU,     0xed871e03U,     0x4a73bb00U, \
    0xa2312868U,     0x04a39a82U,     0xff87e8c1U,     0xc1aa7e45U, \
    0x053bd49fU,     0x21fd83c7U,     0x7fbdfe71U,     0x1916c938U, \
    0x03e60e52U,     0x1e1d8d33U,     0xcd1c36c9U,     0x29829d4eU, \
    0x2a9bcd88U,     0x7b5f6cbeU,     0x793ab2b2U,     0xf57d6a00U, \
    0x1e9d1aadU,     0xcf2a58cdU,     0x804ef45eU,     0xdd4f3babU, \
    0x34ded4f8U,     0xd920c4a2U,     0x852d1959U,     0x681f5e02U, \
    0xb98d4cb1U,     0x2de90611U,     0x8c58b576U,     0x6e37a850U, \
    0x836f7866U,     0x344d4630U,     0x4a18b49fU,     0x7bfabbb9U, \
    0x32d6aec5U,     0x6c848410U,     0x33809a3fU,     0xbc4dfc35U, \
    0x54606ed5U,     0x6d7ecf50U,     0xfa049780U,     0xfd200b8fU, \
    0xa12b98bdU,     0xfd59bd37U,     0xa145ec4aU,     0xc9178b09U, \
    0xb7331472U,     0x5d125e05U,     0xce1d5ae2U,     0xe1f65421U, \
    0xaa55d5eaU,     0x094deb27U,     0x824019dfU,     0x1789662eU, \
    0xb36ed965U,     0x8d4e38d6U,     0x74d61661U,     0x5f16ded4U, \
    0x42d51951U,     0xc8d283b8U,     0x69a94bdeU,     0x028db697U, \
    0x01000103U,     0x30bf81a3U,     0x0c30bc81U,     0x1d550306U, \
    0x30050413U,     0xff010103U,     0x09061230U,     0x0401062bU, \
    0x01268201U,     0x30050403U,     0x01010203U,     0x09065f30U, \
    0x0401062bU,     0x01268201U,     0x30520422U,     0x60090650U, \
    0x65014886U,     0x03020403U,     0x53804004U,     0xdd70616cU, \
    0x08dc2686U,     0xd348f11aU,     0x5dfdc29eU,     0x78c50c09U, \
    0xe74766a7U,     0x4bd33f90U,     0x33432ed0U,     0x0e7be5ecU, \
    0x6f11ff24U,     0x6f9b4243U,     0x4b8341f5U,     0xc8f00ed4U, \
    0xce3a56d3U,     0xd20fedf5U,     0x0102b854U,     0x06183002U, \
    0x01062b09U,     0x26820104U,     0x0b042301U,     0x04040930U, \
    0x00000000U,     0x30020102U,     0x5503061dU,     0x16040e1dU, \
    0xdf041404U,     0x1484b3bbU,     0xcb0c2577U,     0x7da4332eU, \
    0x1259854dU,     0x0d30e5fbU,     0x862a0906U,     0x0df78648U, \
    0x050d0101U,     0x02820300U,     0xf8150001U,     0x1b45d8acU, \
    0x60023618U,     0x3a826169U,     0x359c22c4U,     0x363e6f28U, \
    0x60464422U,     0x737c90f7U,     0x66d86a06U,     0x4795db66U, \
    0xac1510a1U,     0x7aa71c0dU,     0xa8c5703dU,     0xd7801d4aU, \
    0xf0faa35cU,     0xe1d0630cU,     0xf358fe2bU,     0x20694625U, \
    0xec3cbcaaU,     0x8acb3daaU,     0xe4236649U,     0x2e2d6699U, \
    0x3519c331U,     0xb1d440caU,     0xf69a328cU,     0x4d1bffadU, \
    0x808c7ef7U,     0x56e33fe3U,     0x28983deaU,     0x3f157027U, \
    0x565a9c27U,     0xa5483b4dU,     0xcb0aee52U,     0xd1589db9U, \
    0x6945bfe7U,     0xea9834abU,     0xdaf0f277U,     0x1f637352U, \
    0x6bfb0539U,     0xc0227e98U,     0x69afea39U,     0x2bea1b07U, \
    0x7558ca96U,     0xeb37db8cU,     0x0a5aa689U,     0xd7f59b1bU, \
    0x8c9d76a1U,     0x251ca907U,     0x607dfda2U,     0x11a4dc90U, \
    0x77d02d8fU,     0xee03e47eU,     0xef0ce1a2U,     0x20632298U, \
    0x33f0cb8fU,     0xd6ccc252U,     0xa1ca4141U,     0x3c0646daU, \
    0xad6a6fe4U,     0xbf11fb99U,     0xce3f8d19U,     0xac5c915fU, \
    0x114a142aU,     0xe5ec5b67U,     0x9761a910U,     0x30fd06b4U, \
    0x3c4a8ab6U,     0x8c883da1U,     0x526baae9U,     0xf1767e0bU, \
    0x3151f5c2U,     0x307634e9U,     0x50aed82dU,     0x8034f34eU, \
    0x0f5ef740U,     0x4db0e9f3U,     0x92458e3cU,     0x14560846U, \
    0xae2ffa0cU,     0x4e483845U,     0x92795762U,     0x8604cd08U, \
    0x88fe8970U,     0xefc37d82U,     0x265a79d2U,     0xbbdbcd6eU, \
    0xcd71fe30U,     0x81ed88f5U,     0xc2b91caaU,     0xf69af10bU, \
    0xf1f73a0bU,     0x601b7d76U,     0x4bc9827eU,     0x0e4cb1daU, \
    0x57349fd4U,     0xc7806fa4U,     0x1b6f8c51U,     0x81b3788cU, \
    0xe1a78bceU,     0xa14f4925U,     0x3d43a9e8U,     0x3f0c8c7aU, \
    0x45a34575U,     0x0f666e00U,     0xaa12263eU,     0xc1d073d3U, \
    0x20544106U,     0xb0a81980U,     0x419bbaaeU,     0x4e3d01d9U, \
    0x6e221054U,     0xfebbf5e4U,     0x7ae7de7dU,     0x52e25865U, \
    0x0379ff3eU,     0xce22c0eeU,     0xab6901c5U,     0xf198bc2eU, \
    0x2fe5e4f0U,     0x399ad438U,     0xbd9d6d8bU,     0xc039558eU, \
    0x2b61ef79U,     0x969f7b78U,     0x4aab85cdU,     0x59613f5cU, \
    0x979bedecU,     0x62a2b894U,     0x0100be3dU\
} /* 1644 bytes */

#endif
